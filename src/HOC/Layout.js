import React from 'react'
import FooterResponsive from '../Component/Footer/FooterResponsive'
import HeaderResponsive from '../Component/Header/HeaderResponsive'

export default function Layout({ children }) {
  return (
    <div className=''>
        <HeaderResponsive />
        {children}
        <FooterResponsive />
    </div>
  )
}
