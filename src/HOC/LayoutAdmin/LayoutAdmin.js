import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  ApartmentOutlined,
  UserOutlined,
  OrderedListOutlined,
  ScheduleOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import AdminUserNav from "../../Component/Header/AdminUserNav";
import "./LayoutAdmin.css";
const { Header, Sider, Content } = Layout;
export default function LayoutAdmin({ children }) {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();
  return (
    <Layout style={{ height: "100%" }}>
      <Sider trigger={null} collapsible collapsed={collapsed} width={230}>
        <NavLink to="/">
          <div className="logo" />
        </NavLink>
        <Menu
          onClick={({ key }) => {
            if (key === "signout") {
              console.log("signout");
              localStorage.clear();
              window.location.href = "/";
            } else {
              navigate(key);
            }
          }}
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[window.location.pathname]}
          items={[
            {
              key: "/admin/quanlynguoidung",
              icon: <UserOutlined />,
              label: "Quản lý người dùng",
            },
            {
              key: "/admin/quanlycongviec",
              icon: <OrderedListOutlined />,
              label: "Quản lý công việc",
            },
            {
              key: "/admin/quanlyloaicongviec",
              icon: <ApartmentOutlined />,
              label: "Quản lý loại công việc",
            },
            {
              key: "/admin/quanlydichvu",
              icon: <ScheduleOutlined />,
              label: "Quản lý dịch vụ",
            },
            {
              key: "signout",
              icon: <LogoutOutlined />,
              label: "Đăng xuất",
              danger: true,
            },
          ]}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
          <AdminUserNav />
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 30,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
}
