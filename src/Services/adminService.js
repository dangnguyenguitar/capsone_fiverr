import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const adminService = {
  //Users
  getDanhSachNguoiDung: () => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "GET",
      headers: createConfig(),
    });
  },
  postThemNguoiDung: (userData) => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "POST",
      headers: createConfig(),
      data: userData,
    });
  },
  deleteXoaNguoiDung: (userId) => {
    return axios({
      url: `${BASE_URL}/api/users?id=${userId}`,
      method: "DELETE",
      headers: createConfig(),
    });
  },
  getThongTinNguoiDungChiTiet: (userId) => {
    return axios({
      url: `${BASE_URL}/api/users/${userId}`,
      method: "GET",
      headers: createConfig(),
    });
  },
  putCapNhatThongTinNguoiDung: (user) => {
    return axios({
      url: `${BASE_URL}/api/users/${user.id}`,
      method: "PUT",
      headers: createConfig(),
      data: user,
    });
  },
  //Cong viec
  getDanhSachCongViec: () => {
    return axios({
      url: `${BASE_URL}/api/cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getCongViecChiTiet: (jobId) => {
    return axios({
      url: `${BASE_URL}/api/cong-viec/${jobId}`,
      method: "GET",
      headers: createConfig(),
    });
  },
  postThemCongViec: (jobData) => {
    return axios({
      url: `${BASE_URL}/api/cong-viec`,
      method: "POST",
      headers: createConfig(),
      data: jobData,
    });
  },
  putCapNhatCongViec: (job) => {
    return axios({
      url: `${BASE_URL}/api/cong-viec/${job.id}`,
      method: "PUT",
      headers: createConfig(),
      data: job,
    });
  },
  deleteXoaCongViec: (jobId) => {
    return axios({
      url: `${BASE_URL}/api/cong-viec/${jobId}`,
      method: "DELETE",
      headers: createConfig(),
    });
  },
  //Loai cong viec
  getDanhSachLoaiCongViec: () => {
    return axios({
      url: `${BASE_URL}/api/loai-cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },
  //Dich vu
  getDanhSachDichVu: () => {
    return axios({
      url: `${BASE_URL}/api/thue-cong-viec`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
