import axios from "axios"
import { BASE_URL, createConfig } from "./configURL"

export const userService = {
    postDangNhap: (dataUser) => {
        return axios({
            url: `${BASE_URL}/api/auth/signin`,
            method: 'POST',
            data: dataUser,
            headers: createConfig(),
        })
    },

    dangKi: (dataUser) => {
        return axios({
            url: `${BASE_URL}/api/auth/signup`,
            method: 'POST',
            data: dataUser,
            headers: createConfig(),
        })
    },

    employ: (info) => {
        return axios({
            url: `${BASE_URL}/api/thue-cong-viec`,
            method: 'POST',
            data: info,
            headers: createConfig(),
        })
    },

}