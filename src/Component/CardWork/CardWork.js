import React from 'react'
import './card.css'
import { StarFilled } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import ReactPaginate from 'react-paginate';

export default function CardWork({ workArr }) {

  const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 8
  const endOffset = itemOffset + itemsPerPage;

  const currentItems = workArr.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(workArr.length / itemsPerPage);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % workArr.length;
    setItemOffset(newOffset);
  };


    console.log(workArr)
    let renderWorkList = () => {
      return currentItems.map((work)=> {
        return (
          <NavLink key={work.id} to={`/detail-work/${work.id}`}>
            <div className="relative card">
              <div
                style={{
                  backgroundImage: `url('${work.congViec.hinhAnh}')`,
                }} 
               className="banner">
              </div>
              <div className='flex justify-center'>
                  <img className='creator w-20 h-20 object-cover' src={work.avatar} alt="" />
              </div>
              <h2 className="name">{work.tenNguoiTao}</h2>
              <div className="title p-3 font-medium">{work.congViec.tenCongViec}</div>
              <div className="actions">
                <div className="follow-info">
                  <h2><a href="/#"><span className='flex justify-center items-center'><StarFilled className='text-yellow-400 pr-1' />{work.congViec.saoCongViec}</span><small>Rate: {work.congViec.danhGia}</small></a></h2>
                  <h2><a href="/#"><span>${work.congViec.giaTien}</span><small>Price</small></a></h2>
                </div>
                <div className="follow-btn px-10 py-1 absolute bottom-1 w-full">
                  <button>Starting</button>
                </div>
              </div>
            </div>
          </NavLink>
        )
      })
    }
  return (
    <div>
      <div className='grid grid-cols-4 gap-10' >
        {renderWorkList()}
      </div>
      <div className='mt-10'>
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="<"
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName='page-num'
          previousClassName='page-num'
          nextLinkClassName='page-num'
          activeLinkClassName='active-paginate'
        />
      </div>
    </div>
  )
}

