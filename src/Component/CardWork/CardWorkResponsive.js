import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import CardWork from './CardWork'
import CardWorkMobile from './CardWorkMobile'
import CardWorkTablet from './CardWorkTablet'

export default function CardWorkResponsive({ workArr }) {
  return (
    <div>
        <Desktop><CardWork workArr={workArr}/></Desktop>
        <Tablet><CardWorkTablet workArr={workArr}/></Tablet>
        <Mobile><CardWorkMobile workArr={workArr}/></Mobile>
    </div>
  )
}
