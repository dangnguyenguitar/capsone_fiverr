import React from 'react'
import { StarFilled } from '@ant-design/icons';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
import ReactPaginate from 'react-paginate';

export default function CardWorkMobile({ workArr }) {
    const [itemOffset, setItemOffset] = useState(0);
  const itemsPerPage = 8
  const endOffset = itemOffset + itemsPerPage;

  const currentItems = workArr.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(workArr.length / itemsPerPage);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % workArr.length;
    setItemOffset(newOffset);
  };

  let renderWorkList = () => {
    return currentItems.map((work)=> {
      return (
        <NavLink key={work.id} to={`/detail-work/${work.id}`}>
          <div className='mb-4 border-b p-3'>
              <div className='flex'>
                <img className='w-36 h-24 object-cover rounded-md mr-3' src={work.congViec.hinhAnh} alt="" />
                <div>
                  <p className='text-gray-600 font-semibold'>{work.congViec.moTaNgan.length<100?work.congViec.moTaNgan:work.congViec.moTaNgan.slice(0, 40)+"..."}</p>
                  <p  className='mt-5 flex items-center'>
                    <StarFilled className='text-yellow-400 pr-1 text-lg mb-1' />
                    <span className='text-yellow-400 font-semibold text-lg'>{work.congViec.saoCongViec}</span>
                    <span className='text-gray-500 font-semibold ml-2'>({work.congViec.danhGia})</span>
                  </p>
                </div>
              </div>
              <div className='mt-3 flex justify-between items-center'>
                <div className='flex items-center'>
                  <img style={{borderRadius: '50%'}} className='w-8 h-8 object-cover' src={work.avatar} alt="" />
                  <p className='font-semibold text-gray-600 ml-2'>{work.tenNguoiTao}</p>
                </div>
                <div className='flex justify-center items-center'>
                  <small className='text-gray-500 font-semibold mr-2 mt-1'>STARTING AT</small>
                  <p className='text-lg'>US${work.congViec.giaTien}</p>
                </div>
              </div>
          </div>
        </NavLink>
      )
    })
  }
  return (
    <div>
        <div className='' >
            {renderWorkList()}
        </div>
        <div className='mt-10'>
            <ReactPaginate
                breakLabel="..."
                nextLabel=">"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="<"
                renderOnZeroPageCount={null}
                containerClassName="pagination"
                pageLinkClassName='page-num'
                previousClassName='page-num'
                nextLinkClassName='page-num'
                activeLinkClassName='active-paginate'
            />
        </div>
    </div>
  )
}
