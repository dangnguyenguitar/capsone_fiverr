import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import Footer from './Footer'
import FooterMobile from './FooterMobile'
import FooterTablet from './FooterTablet'

export default function FooterResponsive() {
  return (
    <div>
        <Desktop><Footer/></Desktop>
        <Tablet><FooterTablet/></Tablet>
        <Mobile><FooterMobile/></Mobile>
    </div>
  )
}
