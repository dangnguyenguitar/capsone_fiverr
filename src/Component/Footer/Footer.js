import React from 'react'

export default function Footer() {
  return (
    <div>
        <div className="flex w-full justify-center items-center">
            <div className="w-4/5 font-medium text-gray-700 p-8 grid grid-cols-5 grid-flow-col gap-6">
                <div className="">
                <h1>Categories</h1>
                    <p className="font-light text-gray-500 py-4 ">
                    Graphic & Design 
                    <br/>
                    Digital Marketing
                    <br/>
                    Writing & Translation 
                    <br/>
                    Video Animation
                    <br/>Music & Audio 
                    <br/>
                    Programing & Tech
                    <br/>
                    Data
                    <br/>
                    Business
                    <br/>
                    Life Style 
                    <br/>
                    Sitemap
                    </p>
                </div>
            <div>
          <h1>About</h1>
          <p className="font-light text-gray-500 py-4" >
          Careers
            <br/>
            Press & News
            <br/>
            Partnership
            <br/>
            Privacy Policy
            <br/>
            Terms of Service
            <br/>
            Interllectual Property Claims
            <br/>
            Investors Relations
          </p>
        </div>
        <div>
          <h1>Support</h1>
          <p className="font-light text-gray-500 py-4" >
          Help & Support
            <br/>
            Trust & Safety
            <br/>
            Selling on Fiverr
            <br/>
            Buying on Fiverr
            </p>
        </div>
        <div>
          <h1>Community</h1>
          <p className="font-light text-gray-500 py-4" >
          Events
            <br/>
            Blogs
            <br/>
           Forum
            <br/>
            Community Standards
            <br/>
            Podcast
            <br/>
            Affiliates
            <br/>
            Invite a friends
            <br/> Become a Seller
            <br /> Fiverr Evalate
            <div className="text-gray-400 text-xs font-extralight">
             Excusive Benefits </div>

            </p>
        </div>
        <div>
          <h1>More From Fiverr</h1>
          <p className="font-light text-gray-500 py-4" >
          Fiverr Business
            <br/>
            Fiverr Bro
            <br/>
           Fiverr Studio
            <br/>
            Fiverr Logo Maker
            <br/>
            Fiver Guides
            <br/>
            Get Inspired
            <br/>
            Clear Voice
            <div className="text-gray-400 text-xs font-extralight">
             Content marketing
            </div>
            AND CO
            <div className="text-gray-400 text-xs font-extralight">
             Invoice Software </div>
             Learn
            <div className="text-gray-400 text-xs font-extralight">
             Online Courses</div>
            </p>
        </div>
        </div>
      </div>
    </div>
  )
}
