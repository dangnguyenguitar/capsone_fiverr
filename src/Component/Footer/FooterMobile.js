import React from 'react'
import { DownOutlined } from "@ant-design/icons";

export default function FooterMobile() {
  return (
    <div className='p-5 border-t-2 border-gray-300'>
      <p className='flex justify-between items-center'><span className='font-semibold text-xl mb-3 text-gray-700'>Categories</span><DownOutlined /></p>
      <p className='flex justify-between items-center'><span className='font-semibold text-xl mb-3 text-gray-700'>About</span><DownOutlined /></p>
      <p className='flex justify-between items-center'><span className='font-semibold text-xl mb-3 text-gray-700'>Support</span><DownOutlined /></p>
      <p className='flex justify-between items-center'><span className='font-semibold text-xl mb-3 text-gray-700'>Community</span><DownOutlined /></p>
      <p className='flex justify-between items-center'><span className='font-semibold text-xl mb-3 text-gray-700'>More From Fiverr</span><DownOutlined /></p>
    </div>
  )
}
