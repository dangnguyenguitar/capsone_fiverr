import React from 'react'
import { Menu } from 'antd';
import { useState, useEffect } from 'react';
import { https } from '../../Services/configURL'
import { NavLink } from 'react-router-dom';

export default function MenuWorkMobile() {

  function getItem(label, key, children, type) {
    return {
      key,
      children,
      label,
      type,
    };
  }

  let [menuWork, setMenuWork] = useState([])

    useEffect(()=>{
        https.get('/api/cong-viec/lay-menu-loai-cong-viec')
        .then((res) => {
            setMenuWork(res.data.content)
        }).catch((err) => {
            console.log(err)
        });
    },[])

    console.log(menuWork)

  const items = [
    getItem(<p style={{fontSize: 17}} className='text-gray-400 font-semibold mr-5'>Browse Categories</p>, 'sub1', menuWork.map((work)=>{
      return getItem(<p className='text-gray-500 font-semibold'>{work.tenLoaiCongViec}</p>, 
                      work.id,
                      work.dsNhomChiTietLoai?.map((list)=>{
                        return getItem(list.tenNhom, list.id, list.dsChiTietLoai?.map((item)=>{
                          return getItem(<NavLink key={item.id} to={`/detail-categories/${item.id}`}>
                                            <p>{item.tenChiTiet}</p>
                                          </NavLink>, 
                                        item.id,
                                        null)
                        }))
                      })
                    )
    })),
  ];

  const itemsExplore = [
    getItem(<p style={{fontSize: 17}} className='text-gray-400 font-semibold mr-5'>Explore</p>, 'sub2', [
      getItem('Option 1', '1'),
      getItem('Option 2', '2'),
      getItem('Option 3', '3'),
      getItem('Option 4', '4'),
    ]),
  ]

  const rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

  const [openKeys, setOpenKeys] = useState([]);
  const onOpenChange = (keys) => {
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };

  return (
    <div>
      <Menu
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        style={{ width: 240 }}
        items={items}
      />

      <Menu
        mode="inline"
        openKeys={openKeys}
        onOpenChange={onOpenChange}
        style={{ width: 240 }}
        items={itemsExplore}
      />
    </div>
  )
}
