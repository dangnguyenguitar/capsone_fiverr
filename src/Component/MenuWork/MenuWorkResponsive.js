import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import MenuWork from './MenuWork'
import MenuWorkMobile from './MenuWorkMobile'
import MenuWorkTablet from './MenuWorkTablet'

export default function MenuWorkResponsive() {
  return (
    <div>
        <Desktop><MenuWork/></Desktop>
        <Tablet><MenuWorkTablet/></Tablet>
        <Mobile><MenuWorkMobile/></Mobile>
    </div>
  )
}
