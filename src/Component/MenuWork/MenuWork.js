import React, { useEffect, useState } from 'react'
import { https } from '../../Services/configURL'
import { Empty, Popover  } from 'antd';
import { NavLink } from 'react-router-dom';

export default function MenuWork() {
    
    let [menuWork, setMenuWork] = useState([])

    useEffect(()=>{
        https.get('/api/cong-viec/lay-menu-loai-cong-viec')
        .then((res) => {
            setMenuWork(res.data.content)
        }).catch((err) => {
            console.log(err)
        });
    },[])

    console.log(menuWork);

    let renderMenuWork = () => {
        let content
        return (
            <div className='w-11/12'>
                <ul className='flex justify-between mt-1'>
                    {menuWork.map((itemMenu)=>{
                        if (itemMenu.dsNhomChiTietLoai?.length === 0) {
                            content = (
                                <Empty/>
                            )
                        }else{
                            content = (
                                <div className='grid grid-cols-3 gap-10'>
                                {itemMenu.dsNhomChiTietLoai?.map((item)=>{
                                        return (
                                            <div key={item.id} className='p-3'>
                                                <p className='text-lg text-gray-500 mb-2'>{item.tenNhom}</p>
                                                <p>
                                                    {item.dsChiTietLoai?.map((element)=>{
                                                        return (
                                                            <NavLink key={element.id} to={`/detail-categories/${element.id}`}>
                                                                <p className='text-base my-2'>{element.tenChiTiet}</p>
                                                            </NavLink>
                                                        )
                                                    })}
                                                </p>
                                            </div>
                                        )
                                })}
                                </div>
                            );
                        }
                        return (
                            <li className='hover:border-b-green-600 hover:border-b-4 cursor-pointer h-9' key={itemMenu.id}>
                                <Popover placement="bottomLeft" content={content}>
                                    <NavLink to={`/categories/${itemMenu.id}`}><span className='text-lg text-gray-500'>{itemMenu.tenLoaiCongViec}</span></NavLink>
                                </Popover>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
  return (
    <div className='flex justify-center border-y border-gray-300'>{renderMenuWork()}</div>
  )
}
