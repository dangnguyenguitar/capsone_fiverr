import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import Fillter from './Fillter'
import FilterMobile from './FilterMobile'
import FilterTablet from './FilterTablet'

export default function FilterResponsive() {
  return (
    <div>
        <Desktop><Fillter/></Desktop>
        <Tablet><FilterTablet/></Tablet>
        <Mobile><FilterMobile/></Mobile>
    </div>
  )
}
