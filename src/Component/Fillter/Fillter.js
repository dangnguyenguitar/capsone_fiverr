import React from 'react'
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space, Switch ,Popover } from 'antd'

export default function Fillter() {
    const items = [
        {
          label: <a href="/#">1st menu item</a>,
          key: '0',
        },
        {
          label: <a href="/#">2nd menu item</a>,
          key: '1',
        },
        {
          label: '3rd menu item',
          key: '3',
        },
      ];

      const onChange = (checked) => {
        console.log(`switch to ${checked}`);
      };

  return (
    <div>
                <div className='mb-5'>
                    <p className='text-2xl font-semibold'>Result</p>
                    <p className='text-gray-400 font-medium text-lg my-3'>Stand out from the crowd with a logo that fits your brand personality.</p>
                    <div className='flex justify-between items-center my-10'>
                        <div className=''>
                            <Dropdown
                             className='border border-gray-400 px-2 py-1 rounded-sm'
                             menu={{items,}} trigger={['click']} >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        <span className='text-lg font-medium text-gray-700'>Category</span>
                                        <DownOutlined className='mb-1' />
                                    </Space>
                                </a>
                            </Dropdown>

                            <Dropdown
                             className='border border-gray-400 px-2 py-1 mx-3 rounded-sm'
                             menu={{items,}} trigger={['click']} >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        <span className='text-lg font-medium text-gray-700'>Service Options</span>
                                        <DownOutlined className='mb-1' />
                                    </Space>
                                </a>
                            </Dropdown>

                            <Dropdown
                             className='border border-gray-400 px-2 py-1 rounded-sm'
                             menu={{items,}} trigger={['click']} >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        <span className='text-lg font-medium text-gray-700'>Seller Details</span>
                                        <DownOutlined className='mb-1' />
                                    </Space>
                                </a>
                            </Dropdown>

                            <Dropdown
                             className='border border-gray-400 px-2 py-1 m-3 rounded-sm'
                             menu={{items,}} trigger={['click']} >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        <span className='text-lg font-medium text-gray-700'>Budget</span>
                                        <DownOutlined className='mb-1' />
                                    </Space>
                                </a>
                            </Dropdown>

                            <Dropdown
                             className='border border-gray-400 px-2 py-1 rounded-sm'
                             menu={{items,}} trigger={['click']} >
                                <a onClick={(e) => e.preventDefault()}>
                                    <Space>
                                        <span className='text-lg font-medium text-gray-700'>Delivery Time</span>
                                        <DownOutlined className='mb-1' />
                                    </Space>
                                </a>
                            </Dropdown>
                        </div>
                        <div className='flex justify-center items-center'>
                            <Popover className='flex justify-center items-center' placement="bottom" content={(
                                <div className='font-medium'>
                                    <p>Vetted</p>
                                    <p>professionals,</p>
                                    <p>verified for quality</p>
                                    <p>and service.</p>
                                </div>
                            )} >
                                <Switch size='small' className='bg-slate-300 mr-2' onChange={onChange} />
                                <span className='text-gray-500 font-semibold text-lg'>Pro services</span>
                            </Popover>
                            
                            <Popover className='flex justify-center items-center mx-5' placement="bottom" content={(
                                <div className='font-medium'>
                                    <p>Vetted</p>
                                    <p>professionals,</p>
                                    <p>verified for quality</p>
                                    <p>and service.</p>
                                </div>
                            )} >
                                <Switch size='small' className='bg-slate-300 mr-2' onChange={onChange} />
                                <span className='text-gray-500 font-semibold text-lg'>Local sellers</span>
                            </Popover>

                            <Popover className='flex justify-center items-center' placement="bottom" content={(
                                <div className='font-medium'>
                                    <p>Vetted</p>
                                    <p>professionals,</p>
                                    <p>verified for quality</p>
                                    <p>and service.</p>
                                </div>
                            )} >
                                <Switch size='small' className='bg-slate-300 mr-2' onChange={onChange} />
                                <span className='text-gray-500 font-semibold text-lg'>Online sellers</span>
                            </Popover>
                        </div>
                    </div>
                </div>
    </div>
  )
}
