import React from 'react'
import { NavLink } from 'react-router-dom'
import UserNav from './UserNav'
import { Input, message  } from 'antd';
import { useNavigate } from 'react-router-dom';
import { SearchOutlined,  } from '@ant-design/icons';
import MenuWorkResponsive from '../MenuWork/MenuWorkResponsive';

export default function Header() {
  const { Search } = Input;
    let navigate = useNavigate()
    const onSearch = (value) => {
        if(value.trim() === ''){
          message.error('Please input')
        }else{
          navigate(`/work-list/${value}`)
        }
    };

  return (
    <div>
      <div className='fixed bg-white top-0 w-full z-20'>
          <div className='w-full flex justify-center items-center'>
            <div className='flex justify-between items-center w-11/12'>
              <div className='flex w-5/12'>
                <NavLink className='py-7'  to={'/'}>
                  <p className='font-bold text-4xl text-gray-800'><a href="/">fiverr</a><span className='text-4xl text-green-600'>.</span></p>
                </NavLink>
                <div id='searchNav' className='m-5 w-full px-10'>
                  <Search
                    placeholder="What service are you looking for today?"
                    enterButton={<SearchOutlined className='mb-2 text-xl' />}
                    allowClear
                    size='large'
                    onSearch={onSearch}
                    autoComplete='off'
                    className='bg-neutral-800 font-semibold rounded-md mt-3'
                    />
                </div>
              </div>
              <UserNav/>
            </div>
          </div>
          <div className='w-full'>
            <MenuWorkResponsive/>
          </div>
        </div>
    </div>
  )
}
