import React from "react";
import { Avatar } from "antd";
import { useSelector } from "react-redux";
export default function AdminUserNav() {
  const userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });
  console.log("userInfor: ", userInfor);
  if (userInfor.user.role === "ADMIN") {
    return (
      <div className="px-24">
        <Avatar
          style={{
            backgroundColor: "teal",
            verticalAlign: "middle",
          }}
          size="large"
          shape="square"
        >
          {userInfor.user.name}
        </Avatar>
      </div>
    );
  } else {
    alert("Đã có lỗi xảy ra! Người dùng không phải là admin.");
    window.location.href = "/";
  }
}
