import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalService } from "../../Services/localStorageService";
import { Dropdown, Space } from "antd";

export default function UserNav() {
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });

  let handleLogout = () => {
    userLocalService.remove();
    window.location.href = "/";
  };

  let renderContent = () => {
    if (userInfor) {
      const items = [
        {
          label: (
            <NavLink to={`/user/${userInfor.user.id}`}>
              <span className="">Account information</span>
            </NavLink>
          ),
          key: "0",
        },
        renderAdminTab(),
        {
          type: "divider",
        },
        {
          label: <button onClick={handleLogout}>Log Out</button>,
          key: "2",
        },
      ];
      return (
        <div>
          <Dropdown
            menu={{
              items,
            }}
            trigger={["click"]}
          >
            <a onClick={(e) => e.preventDefault()}>
              <Space className="">
                <p className="text-lg cursor-pointer">{userInfor.user.name}</p>
              </Space>
            </a>
          </Dropdown>
        </div>
      );
    } else {
      return (
        <div>
          <NavLink className={"mr-3"} to={"/login"}>
            <button className="font-semibold">Sign In</button>
          </NavLink>
          <NavLink to={"/register"}>
            <button className="border-black border px-5 py-0.5 rounded-md font-semibold">
              Join
            </button>
          </NavLink>
        </div>
      );
    }
  };

  let renderAdminTab = () => {
    if (userInfor.user.role === "ADMIN") {
      return {
        label: (
          <NavLink to="/admin/quanlynguoidung">
            <span>Admin dashboard</span>
          </NavLink>
        ),
        key: "3",
      };
    } else {
      return null;
    }
  };
  return (
    <div className="flex items-center">
      <div className="mr-5">
        <a href="/#">Become a Seller</a>
      </div>
      <div>{renderContent()}</div>
    </div>
  );
}
