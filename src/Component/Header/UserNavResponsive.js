import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import UserNav from './UserNav'
import UserNavMobile from './UserNavMobile'
import UserNavTablet from './UserNavTablet'

export default function UserNavResponsive() {
  return (
    <div>
        <Desktop><UserNav/></Desktop>
        <Tablet><UserNavTablet/></Tablet>
        <Mobile><UserNavMobile/></Mobile>
    </div>
  )
}
