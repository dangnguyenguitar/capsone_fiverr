import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import Header from './Header'
import HeaderMobile from './HeaderMobile'
import HeaderTablet from './HeaderTablet'

export default function HeaderResponsive() {
  return (
    <div>
        <Desktop><Header/></Desktop>
        <Tablet><HeaderTablet/></Tablet>
        <Mobile><HeaderMobile/></Mobile>
    </div>
  )
}
