import React, { useState } from 'react'
import {  Drawer, message, Input } from 'antd';
import { NavLink, useNavigate } from 'react-router-dom';
import MenuWorkMobile from '../MenuWork/MenuWorkMobile';
import { MenuOutlined, SearchOutlined } from '@ant-design/icons';
import UserNavResponsive from './UserNavResponsive';

export default function HeaderMobile() {
    const [open, setOpen] = useState(false);
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };

  let cssSearch = ''

  const { Search } = Input;
    let navigate = useNavigate()
    const onSearch = (value) => {
        if(value.trim() === ''){
          message.error('Please input')
        }else{
          navigate(`/work-list/${value}`)
        }
    };

    if(window.location.pathname === '/'){
      cssSearch = 'hidden';
    }else{
      cssSearch = 'block'
    }


  return (
    <div 
    style={{boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'}}
    className='w-full fixed z-20 bg-white top-0 left-0'>
        <div className='flex justify-between items-center'>
            <div>
                <button className='p-3' onClick={showDrawer}>
                    <MenuOutlined className='text-2xl font-extrabold text-gray-700' />
                </button>
                <Drawer closable={false} width='270' placement="left" onClose={onClose} open={open}>
                    <button className='bg-green-600 text-white font-semibold text-lg px-5 py-2 rounded-md'><NavLink to={'/register'}>Join Fiverr</NavLink></button>
                    <p style={{fontSize: 16}} className='mt-5 text-gray-400 font-semibold ml-6 mb-2'><NavLink to={'/login'}>Sign In</NavLink></p>
                    <MenuWorkMobile/>
                    <p style={{fontSize: 16}} className='text-green-500 font-semibold ml-6 mt-2 mb-10'>Fiverr Business</p>
                    <p className='font-semibold' >General</p>
                    <hr className='h-0.5 my-3' />
                    <p style={{fontSize: 16}} className='text-gray-400 font-semibold'><a href="/">Home</a></p>
                    <p style={{fontSize: 16}} className='text-gray-400 font-semibold my-3' >English</p>
                    <p style={{fontSize: 16}} className='text-gray-400 font-semibold' >US$ USD</p>
                </Drawer>
            </div>
            <div>
                <p className='font-bold text-4xl text-gray-800'><a href="/">fiverr</a><span className='text-4xl text-green-600'>.</span></p>
            </div>
            <div className='m-3 mt-5'>
                <UserNavResponsive/>
            </div>
        </div>
            <div className='flex justify-center items-center'>
                <div className={`m-4 w-5/6 ${cssSearch}`}>
                  <Search
                    placeholder="What service are you looking for today?"
                    enterButton={<SearchOutlined className='mb-1 text-lg' />}
                    allowClear
                    size='large'
                    onSearch={onSearch}
                    autoComplete='off'
                    className='bg-neutral-800 font-semibold rounded-md'
                    />
                </div>
            </div>
    </div>
  )
}