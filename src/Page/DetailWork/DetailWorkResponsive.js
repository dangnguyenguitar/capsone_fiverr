import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import DetailWork from './DetailWork'
import DetailWorkMobile from './DetailWorkMobile'
import DetailWorkTablet from './DetailWorkTablet'

export default function DetailWorkResponsive() {
  return (
    <div>
        <Desktop><DetailWork/></Desktop>
        <Tablet><DetailWorkTablet/></Tablet>
        <Mobile><DetailWorkMobile/></Mobile>
    </div>
  )
}
