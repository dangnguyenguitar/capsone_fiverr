import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { BASE_URL, createConfig, https } from '../../Services/configURL'
import { RightOutlined, LikeOutlined, DislikeOutlined, MessageOutlined } from '@ant-design/icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Avatar, Divider, List, Skeleton, Input, Rate, Form, Button, message } from 'antd';
import moment from 'moment/moment';
import profile from '../../assets/profile.json'
import Lottie from 'lottie-react';
import axios from 'axios';
import swal from 'sweetalert';
import BookingWork from '../BookingWork/BookingWork';

export default function DetailWorkMobile() {
    let [detailWork, setDetailWork] = useState([])
  let params = useParams()
  // let [comment, setComment] = useState([])
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(()=>{
    https.get(`/api/cong-viec/lay-cong-viec-chi-tiet/${params.id}`)
    .then((res) => {
      setDetailWork(res.data.content)
    }).catch((err) => {
      console.log(err);      
    });
  },[params.id])

  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    // https.get(`/api/binh-luan/lay-binh-luan-theo-cong-viec/${params.id}`)
    axios({
      url: `${BASE_URL}/api/binh-luan/lay-binh-luan-theo-cong-viec/${params.id}`,
      method: 'GET',
      headers: createConfig()
    })
      .then((res) => {
        setData([...data, ...res.data.content]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
  }, []);

  const onFinish = (values) => {
    console.log(values);
    if (!localStorage.getItem("USER_LOGIN")) {
      swal({
        title: "You are not logged in",
        text: "Please login to comment!",
        icon: "warning",
        buttons: "OK"
      })
    } else {
      axios({
        url: `${BASE_URL}/api/binh-luan`,
        method: 'POST',
        data: values,
        headers: createConfig(),
      })
      .then((res) => {
        message.success('Success')
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }).catch((err) => {
        message.error("Error")
        console.log(err);
      });
    }
  }

  console.log(detailWork)
  const { TextArea } = Input;

  let renderUserInfor = () => {
    if (!localStorage.getItem("USER_LOGIN")) {
      return (
        <div>
                    <Form
                      className='-mt-24'
                      onFinish={onFinish}
                    >
                      <Form.Item name='id' initialValue='0'></Form.Item>
                      <Form.Item name='maCongViec' initialValue={params.id}></Form.Item>
                      <Form.Item
                        name='saoBinhLuan'
                      >
                        <Rate />
                      </Form.Item>
                      <Form.Item
                        name='noiDung'
                      >
                        <TextArea placeholder='Your comment ...' rows={4} />
                      </Form.Item>
                      <Form.Item name='ngayBinhLuan' initialValue={moment().format("DD/MM/YYYY")}></Form.Item>
                      <Form.Item name='maNguoiBinhLuan' initialValue='0' ></Form.Item>
                      <Form.Item 
                        className='-mt-24 flex'
                      >
                        <Button htmlType="submit" className="bg-blue-500 text-white hover:bg-white w-full mb-2">
                          Comment
                        </Button>
                      </Form.Item>
                    </Form>
        </div>
      )
    }else {
      const info = JSON.parse(localStorage.getItem("USER_LOGIN"))
      if (info.user.avatar === ''){
        return (
          <div>
            <div>
              <div>
                <Lottie className='w-16 h-16' animationData={profile} loop={true} />
              </div>
              <p className='ml-3 text-lg font-medium'>{info.user.name}</p>
            </div>
              <div>
                  <Form
                    className='-mt-24'
                      onFinish={onFinish}
                    >
                      <Form.Item name='id' initialValue='0'></Form.Item>
                      <Form.Item name='maCongViec' initialValue={params.id}></Form.Item>
                      <Form.Item
                        name='saoBinhLuan'
                      >
                        <Rate />
                      </Form.Item>
                      <Form.Item
                        name='noiDung'
                      >
                        <TextArea placeholder='Your comment ...' rows={4} />
                      </Form.Item>
                      <Form.Item name='ngayBinhLuan' initialValue={moment().format("DD/MM/YYYY")}></Form.Item>
                      <Form.Item name='maNguoiBinhLuan' initialValue={info.user.id} ></Form.Item>
                      <Form.Item 
                        className='-mt-24 flex'
                      >
                        <Button htmlType="submit" className="bg-blue-500 text-white hover:bg-white w-full mb-2">
                          Comment
                        </Button>
                      </Form.Item>
                    </Form>
              </div>
          </div>
        )
      } else {
        return (
          <div>
            <div>
              <img style={{
                    borderRadius: '50%'
                  }} className='w-16 h-16' src={info.user.avatar} alt="" />
            </div>
            <p className='ml-3 text-lg font-medium'>{info.user.name}</p>
          </div>
        )
      }
    }
    
  }

  let renderDetailWork = () => {
    return detailWork.map((item)=> {
      return (
        <div className='mt-3 p-5 w-full'>
          <div className='border-b-2 border-gray-300 py-3 mb-5'>
            <ul className='flex items-center'>
              <li><NavLink><p className='text-sm text-blue-600 hover:text-gray-600'>{item.tenLoaiCongViec}</p></NavLink></li>
              <RightOutlined className='mt-1' />
              <li><NavLink><p className='text-sm text-blue-600 hover:text-gray-600'>{item.tenNhomChiTietLoai}</p></NavLink></li>
              <RightOutlined className='mt-1' />
              <li><NavLink><p className='text-sm text-blue-600 hover:text-gray-600'>{item.tenChiTietLoai}</p></NavLink></li>
            </ul>
          </div>
          <div className=''>
            <div className=''>
              <p className='text-2xl text-gray-600 font-bold'>{item.congViec.tenCongViec}</p>
              <div className='border-b border-gray-300 p-2'>
                <img style={{
                  borderRadius: '50%'
                }} className='w-10 h-10' src={item.avatar} alt="" />
                <p className='font-medium'>{item.tenNguoiTao}</p>
                <p className='text-orange-500'>Top Rated Seller <span className='font-bold text-gray-500 mx-1'>|</span></p>
                <p className='mr-1'><Rate className='mr-2 mb-1' disabled allowClear={false} defaultValue={item.congViec.saoCongViec} />{item.congViec.saoCongViec}</p>
                <p className='mr-2'>({item.congViec.danhGia}) <span className='font-bold text-gray-500 mx-1'>|</span></p>
                <p className='w-40 p-2 bg-blue-900 rounded text-white font-medium'>FIVERR'S <span className='text-green-500'>CHOICE</span></p>
              </div>
              <div className='flex items-center p-3'>
                <img className='w-12 h-16' src="https://fiverr-res.cloudinary.com/image/upload/f_auto,q_auto/v1/attachments/generic_asset/asset/56ff3db8ae625ba1d6493c3c250c5919-1625663632464/3-Trophy-70_alpha.gif" alt="" />
                <p className='font-medium mx-2'>People keep coming back!</p>
                <p>This seller has many repeat buyers.</p>
              </div>
              <div>
                <img className='w-full object-cover mb-3' src={item.congViec.hinhAnh} alt="" />
                <BookingWork detailWork={detailWork}/>
                <div className='my-10'>
                  <h2 className='text-xl font-bold text-gray-900' >About This Gig</h2>
                  <p className='text-lg my-5 font-bold text-gray-600'>Description:</p>
                  <p className='text-lg mb-5 text-gray-500'>{item.congViec.moTaNgan}</p>
                    <p className=' text-gray-500'>{item.congViec.moTa}</p>
                </div>
              </div>
              <div>
                <h2 className='text-xl font-bold text-gray-900'>About The Seller</h2>
                <div className='flex mt-8'>
                  <img style={{
                      borderRadius: '50%'
                    }} className='w-32 h-32' src={item.avatar} alt="" />
                  <div className='ml-5'>
                    <p className='mx-3 font-medium text-xl'>{item.tenNguoiTao}</p>
                    <p className='mr-1 text-orange-500 mb-5'>
                      <Rate className='mr-2 mb-1' disabled allowClear={false} defaultValue={item.congViec.saoCongViec} />{item.congViec.saoCongViec}
                      <span className='ml-2 text-gray-600'>({item.congViec.danhGia})</span>
                    </p>
                    <button className='hover:bg-slate-400 hover:text-white px-3 py-2 rounded border border-gray-700 font-semibold text-gray-600' >Contact Me</button>
                  </div>
                </div>
              </div>
              <div className='mt-10'>
                <h2 className='text-xl font-bold text-gray-900'>Comment</h2>
                <div className='my-5'>
                  <div
                    id="scrollableDiv"
                    style={{
                      height: 300,
                      overflow: 'auto',
                      padding: '0 16px',
                      borderTop: '1px solid rgba(140, 140, 140, 0.35)',
                    }}
                    >
                    <InfiniteScroll
                      dataLength={data.length}
                      next={loadMoreData}
                      loader={
                        <Skeleton
                          avatar
                          paragraph={{
                            rows: 1,
                          }}
                          active
                        />
                      }
                      endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
                      scrollableTarget="scrollableDiv"
                    >
                      <List
                        dataSource={data}
                        renderItem={(item) => (
                          <List.Item>
                            <List.Item.Meta
                              avatar={<Avatar src={item.avatar} />}
                              title={
                                <div className='w-56'>
                                  <div className='flex'>
                                    <p className='font-medium w-32 text-xs'>{item.tenNguoiBinhLuan}</p>
                                    <p className='text-xs'>{item.ngayBinhLuan}</p>
                                  </div>
                                  <p className='mr-1 text-orange-500'>
                                        <Rate className='mr-2 mb-1 text-xs' disabled allowClear={false} defaultValue={item.saoBinhLuan} />
                                    </p>
                                </div>
                              }
                              description={
                                <div className='w-56'><p>{item.noiDung}</p></div>
                              }
                            />
                            <div className='mt-24 relative flex items-center'>
                              <p><MessageOutlined/></p>
                              <p className='mx-5'><LikeOutlined />Helpful</p>
                              <p><DislikeOutlined/> Not Helpful</p>
                            </div>
                          </List.Item>
                        )}
                      />
                    </InfiniteScroll>
                  </div>
                  <div className='mt-10'>
                    <div>
                      {renderUserInfor()}
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    })
  }
  return (
    <div className='flex justify-center items-center mt-28'>
        {renderDetailWork()}
    </div>
  )
}
