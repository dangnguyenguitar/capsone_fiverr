import React from 'react'
import LayoutTablet from './Layout/LayoutTablet'
import SearchResponsive from './Search/SearchResponsive'

export default function HomePageTablet() {
  return (
    <div>
        <SearchResponsive/>
        <LayoutTablet/>
    </div>
  )
}
