import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import HomePage from './HomePage'
import HomePageMobile from './HomePageMobile'
import HomePageTablet from './HomePageTablet'

export default function HomePageResponsive() {
  return (
    <div>
        <Desktop><HomePage/></Desktop>
        <Tablet><HomePageTablet/></Tablet>
        <Mobile><HomePageMobile/></Mobile>
    </div>
  )
}
