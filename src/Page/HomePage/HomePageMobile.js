import React from 'react'
import LayoutMobile from './Layout/LayoutMobile'
import SearchResponsive from './Search/SearchResponsive'

export default function HomePageMobile() {
  return (
    <div className='mt-14'>
        <SearchResponsive/>
        <LayoutMobile/>
    </div>
  )
}
