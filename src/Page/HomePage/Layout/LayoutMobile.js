import React from 'react'
import {
    ARTICLE_FIVERR,
    LOGO_DESIGN,
    SOCIAL_MEDIA,
    VIDEO_EXPLAINER,
    VOICE_OVER,
    WORD_PRESS,
    VERIFY_ICON,
    ARTICLE_FIVERR2,
    GRAPHIC_DESIGN,
    DIGITAL_MARKETING,
    WRITING_TRANSLATION,
    VIDEO_ANIMATION,
    MUSIC_AUDIO,
    PROGRAMING_TECH,
    BUSINESS,
    LIFE_STYLE,
    DATA,
  } from "../../../constants/images";

  import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
    import "swiper/css";
    import "swiper/css/navigation";
    // import required modules
    import { Navigation } from "swiper";

export default function LayoutMobile() {
  return (
    <div>
        <div className='p-5'>
            <h1 className="mt-5 font-semibold text-3xl text-gray-700 mb-5">
                Popular professonal services
            </h1>
            <div>
            <Swiper navigation={true} modules={[Navigation]} className="mySwiper">
                <SwiperSlide>
                    <p className="absolute text-white ml-3 mt-2 text-sm">
                        Buil your brand
                    </p>
                    <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                        Logo Design
                    </p>
                    <img
                        style={{height: 345}}
                        className="w-full object-cover"
                        src={LOGO_DESIGN}
                        alt=""
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <p className="absolute text-white text-sm ml-3 mt-2">
                    Customize your site
                    </p>
                    <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                        WordPress
                    </p>
                    <img
                        className="w-full object-cover"
                        style={{ height: 345 }}
                        src={WORD_PRESS}
                        alt=""
                    />
                </SwiperSlide>
                <SwiperSlide>
                    <p className="absolute text-white ml-3 text-sm mt-2">
                        Share your message
                    </p>
                    <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                        Voice Over
                    </p>
                    <img
                        className="w-full object-cover"
                        style={{ height: 345 }}
                        src={VOICE_OVER}
                        alt=""
                    />
                </SwiperSlide>
                <SwiperSlide>
                <p className="absolute text-white ml-3 text-sm mt-2">
                    Engage your audience
                </p>
                <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                    Video Explainer
                </p>
                <img
                    className="w-full object-cover"
                    style={{ height: 345 }}
                    src={VIDEO_EXPLAINER}
                    alt=""
                />
                </SwiperSlide>
                <SwiperSlide>
                <p className="absolute text-white ml-3 mt-2 text-sm">
                    Reach more customer
                </p>
                <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                    Social Media
                </p>
                <img
                    className="w-full object-cover"
                    style={{ height: 345 }}
                    src={SOCIAL_MEDIA}
                    alt=""
                />
                </SwiperSlide>
            </Swiper>
            </div>
        </div>
        <div className="bg-[#f1fdf7] p-5">
          <div className=" text-gray-700 w-full text-left my-5">
            <h1 className="font-medium text-3xl">
              A whole world of freelance <br /> talent at your fingertips
            </h1>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} alt='' />
                <h2 className="text-xl font-semibold pt-4">The best for every budget</h2>
              </div>
              <div>
                <p className="text-gray-600 text-xl">
                  Find high-quality services at every price point. No hourly{" "}
                  <br /> rates, just project-based pricing
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} alt='' />
                <h2 className="text-xl font-semibold pt-4">Quality work done quickly</h2>
              </div>
              <div>
                <p className="text-gray-600 text-xl">
                  Find the right freelancer to begin working on your project
                  <br /> within minutes.
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} alt='' />
                <h2 className="text-xl font-semibold pt-4">
                  Protected payments, every time
                </h2>
              </div>
              <div>
                <p className="text-gray-600 text-xl">
                  Always know what you'll pay upfront. Your payment isn't
                  released
                  <br /> until you approve the work.
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} alt='' />
                <h2 className="text-xl font-semibold pt-4">24/7 support</h2>
              </div>
              <div>
                <p className="text-gray-600 text-xl">
                  Questions? Our round-the-clock support team is available
                  <br /> to help anytime, anywhere.
                </p>
              </div>
            </div>
          </div>
          <div className="mb-5">
                <img
                className="w-full text- alt=''right object-cover"
                style={{ width: 450, height: 200 }}
                src={ARTICLE_FIVERR}
                alt=''
                />
            </div>
        </div>
        <div className="p-5" >
            <div className="text-left">
                <div> 
                    <p className="text-sm text-gray-600 font-medium pb-8">Kay Kim, Co-founder <span className="font-extrabold text-black">| rooted</span> </p>
                </div>
                <div>
                    <p className="text-green-800 italic">"It's extremely exiting that Fiverr has freelancers<br/> from all over the world - it broadends the talent pool.
                    <br/>One of the best things about Fiverr is that while we're <br/> sleeping, someone working."</p>
                </div>
            </div>
            <div className="mt-3">
                <div>
                    <img className="w-full object-cover" src={ARTICLE_FIVERR2} alt=''/>
                </div>
            </div>
       </div>
       <div className='p-5'>
            <div>
                <h1 className="font-medium text-gray-600 text-xl">Explore the marketplace</h1>
            </div>
            <div className="grid grid-cols-2 ml-5 mt-3">
                <img src={GRAPHIC_DESIGN} />
                <img className="" src={PROGRAMING_TECH} />
                <img src={DIGITAL_MARKETING} />
                <img className="" src={BUSINESS} />
                <img src={WRITING_TRANSLATION} />
                <img className="" src={LIFE_STYLE} />
                <img src={VIDEO_ANIMATION} />
                <img className="" src={DATA} />
                <img src={MUSIC_AUDIO} />
            </div>
       </div>
    </div>
  )
}
