import React from "react";
import {
  ARTICLE_FIVERR,
  LOGO_DESIGN,
  SOCIAL_MEDIA,
  VIDEO_EXPLAINER,
  VOICE_OVER,
  WORD_PRESS,
  VERIFY_ICON,
  ARTICLE_FIVERR2,
  GRAPHIC_DESIGN,
  DIGITAL_MARKETING,
  WRITING_TRANSLATION,
  VIDEO_ANIMATION,
  MUSIC_AUDIO,
  PROGRAMING_TECH,
  BUSINESS,
  LIFE_STYLE,
  DATA,
} from "../../../constants/images";

export default function Layout() {
  return (
    <div>
      <div
        style={{ width: "100%", height: "86px" }}
        className="bg-slate-100"
      ></div>
      <div>
        {" "}
        <h1 className="flex pt-10 pl-48 font-bold text-xl text-gray-700">
          Popular professonal services
        </h1>
      </div>
      <div className="flex cursor-pointer">
        <div className="w-2/12 flex items-center">
          <div className="w-full text-right ">
            <button className="p-3 rounded-full bg-white shadow-lg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                />
              </svg>
            </button>
          </div>
        </div>
        <div id="sliderContainer" className="w-full ">
          <ul id="slider" className="flex w-full">
            <li className="p-5">
              <p className="absolute text-white ml-3 mt-2 text-sm">
                Buil your brand
              </p>
              <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                Logo Design
              </p>
              <img
                className="h-50 w-full object-cover"
                style={{ width: 220, height: 280 }}
                src={LOGO_DESIGN}
                alt=""
              />
            </li>
            <li className="p-5">
              <p className="absolute text-white text-sm ml-3 mt-2">
                Customize your site
              </p>
              <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                WordPress
              </p>
              <img
                className="h-50 w-full object-cover"
                style={{ width: 220, height: 280 }}
                src={WORD_PRESS}
                alt=""
              />
            </li>
            <li className="p-5">
              <p className="absolute text-white ml-3 text-sm mt-2">
                Share your message
              </p>
              <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                Voice Over
              </p>
              <img
                className="h-50 w-full object-cover"
                style={{ width: 220, height: 280 }}
                src={VOICE_OVER}
                alt=""
              />
            </li>
            <li className="p-5">
              <p className="absolute text-white ml-3 text-sm mt-2">
                Engage your audience
              </p>
              <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                Video Explainer
              </p>
              <img
                className="h-50 w-full object-cover"
                style={{ width: 220, height: 280 }}
                src={VIDEO_EXPLAINER}
                alt=""
              />
            </li>
            <li className="p-5">
              <p className="absolute text-white ml-3 mt-2 text-sm">
                Reach more customer
              </p>
              <p className="absolute text-white ml-3 mt-8 text-xl font-medium">
                Social Media
              </p>
              <img
                className="h-50 w-full object-cover"
                style={{ width: 220, height: 280 }}
                src={SOCIAL_MEDIA}
                alt=""
              />
            </li>
          </ul>
        </div>
        <div className="w-2/12 flex items-center">
          <div className="w-full text-left">
            <button className="p-3 rounded-full bg-white shadow-lg ml-5 ">
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
      <br /> <br />
      <div
        style={{ width: "100%", height: "500px" }}
        className="bg-[#F2FEF8] flex"
      >
        <div className="w-6/12 p-10 items-center">
          <div className=" w-full ml-28 text-left">
            <h1 className="font-medium text-2xl">
              A whole world of freelance <br /> talent at your fingertips
            </h1>
            {/* content art*/}
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} />
                <h2 className=" font-bold pt-4">The best for every budget</h2>
              </div>
              <div>
                <p className="text-gray-600 text-sm">
                  Find high-quality services at every price point. No hourly{" "}
                  <br /> rates, just project-based pricing
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} />
                <h2 className=" font-bold pt-4">Quality work done quickly</h2>
              </div>
              <div>
                <p className="text-gray-600 text-sm">
                  Find the right freelancer to begin working on your project
                  <br /> within minutes.
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} />
                <h2 className=" font-bold pt-4">
                  Protected payments, every time
                </h2>
              </div>
              <div>
                <p className="text-gray-600 text-sm">
                  Always know what you'll pay upfront. Your payment isn't
                  released
                  <br /> until you approve the work.
                </p>
              </div>
            </div>
            <div>
              <div className="flex ">
                <img className="pt-4 mr-2" src={VERIFY_ICON} />
                <h2 className=" font-bold pt-4">24/7 support</h2>
              </div>
              <div>
                <p className="text-gray-600 text-sm">
                  Questions? Our round-the-clock support team is available
                  <br /> to help anytime, anywhere.
                </p>
              </div>
            </div>
            {/*  */}
          </div>
        </div>
        <div className=" w-6/12 p-10 items-center">
          <div>
            <img
              className="w-full text-right "
              style={{ width: 450, height: 260 }}
              src={ARTICLE_FIVERR}
            />
          </div>
        </div>
      </div>
      <br /> <br />
      <div>
      <div className="grid grid-cols-2 gap-2">
       <div className=" flex items-center col-span-1">
        <div className="ml-28  "><button className="p-3 rounded-full bg-white shadow-lg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                />
              </svg>
            </button></div>
        <div>
          <img style={{width:500, height:280}}className="ml-4" src={ARTICLE_FIVERR2}/>
        </div>
       </div>
       <div className=" flex items-center" >
        <div className="text-left pr-32">
          <div> 
            <p className="text-sm text-gray-600 font-medium pb-8">Kay Kim, Co-founder <span className="font-extrabold text-black">| rooted</span> </p>
          </div>
          <div>
            <p className="text-green-800 italic">"It's extremely exiting that Fiverr has freelancers<br/> from all over the world - it broadends the talent pool.
            <br/>One of the best things about Fiverr is that while we're <br/> sleeping, someone working."</p>
          </div>
        </div>
        <div className="text-right">
        <button className="p-3 rounded-full bg-white shadow-lg ml-5 ">
              {" "}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                class="w-6 h-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
                />
              </svg>
            </button>
        </div>
       </div>
       </div>
      <div>
        <h1 className="pl-28 pt-28 font-medium text-gray-600 text-xl">Explore the marketplace</h1>
      </div>
      <div className="pl-28 py-8 grid grid-rows-2 grid-flow-col">
        <img src={GRAPHIC_DESIGN} />
        <img className="pt-4" src={PROGRAMING_TECH} />
        <img src={DIGITAL_MARKETING} />
        <img className="pt-4" src={BUSINESS} />
        <img src={WRITING_TRANSLATION} />
        <img className="pt-4" src={LIFE_STYLE} />
        <img src={VIDEO_ANIMATION} />
        <img className="pt-4" src={DATA} />
        <img src={MUSIC_AUDIO} />
      </div>
      </div>

      <br/>
      <hr style={{width:"100%", textAlign:"left",marginLeft:"0"}} />
      <div>
        
        </div>
    </div>
  );
}
