import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../HOC/Responsive'
import Layout from './Layout'
import LayoutMobile from './LayoutMobile'
import LayoutTablet from './LayoutTablet'

export default function LayoutResponsive() {
  return (
    <div>
        <Desktop><Layout/></Desktop>
        <Tablet><LayoutMobile/></Tablet>
        <Mobile><LayoutTablet/></Mobile>
    </div>
  )
}
