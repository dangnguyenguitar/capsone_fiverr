import React from 'react'
import Carousel from './Carosel/Carousel'
import Layout from './Layout/Layout'
import SearchResponsive from './Search/SearchResponsive'

export default function HomePage() {
  return (
    <div className='relative'> 
      <SearchResponsive/>
      <Carousel/>
      <Layout/>
    </div>
  )
}
