import React from 'react'
import { Input, message } from 'antd';
import { SearchOutlined,  } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
export default function Search() {
    const { Search } = Input;
    let navigate = useNavigate()
    const onSearch = (value) => {
        if(value.trim() === ''){
            message.error('Please input')
          }else{
            navigate(`/work-list/${value}`)
          }
    };
  return (
    <div className='flex justify-center'>
        <div className='flex items-center z-10 absolute translate-y-3/4 w-11/12'>
            <div className='w-1/2 mt-10'>
                <h1 className='text-5xl font-bold text-white '>Find the perfect <i className='font-sans font-semibold'>freelance</i> services for your business</h1>
                <div className='my-7'>
                    <Search
                    prefix={<SearchOutlined className='p-2' />}
                    placeholder="Try 'building mobile app' "
                    allowClear
                    enterButton="Search"
                    size='large'
                    onSearch={onSearch}
                    autoComplete='off'
                    className='bg-green-500 font-semibold rounded-md w-5/6'
                    />
                </div>
                <div className='flex text-white'>
                    Popular:
                    <ul className='flex'>
                        <li><a className='transition hover:bg-white hover:text-black font-semibold mx-2 px-3 py-1 rounded-2xl text-sm border border-white' href="/#">Website Design</a></li>
                        <li><a className='transition hover:bg-white hover:text-black font-semibold mx-2 px-3 py-1 rounded-2xl text-sm border border-white' href="/#">WordPress</a></li>
                        <li><a className='transition hover:bg-white hover:text-black font-semibold mx-2 px-3 py-1 rounded-2xl text-sm border border-white' href="/#">Logo Design</a></li>
                        <li><a className='transition hover:bg-white hover:text-black font-semibold mx-2 px-3 py-1 rounded-2xl text-sm border border-white' href="/#">Video Editing</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  )
}
