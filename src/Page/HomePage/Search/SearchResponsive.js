import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../HOC/Responsive'
import Search from './Search'
import SearchMobile from './SearchMobile'
import SearchTablet from './SearchTablet'


export default function SearchResponsive() {
  return (
    <div>
        <Desktop><Search/></Desktop>
        <Tablet><SearchTablet/></Tablet>
        <Mobile><SearchMobile/></Mobile>
    </div>
  )
}
