import React from 'react'
import { Input, message } from 'antd';
import { SearchOutlined,  } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

export default function SearchMobile() {

    const { Search } = Input;
    let navigate = useNavigate()
    const onSearch = (value) => {
        if(value.trim() === ''){
            message.error('Please input')
          }else{
            navigate(`/work-list/${value}`)
          }
    };

  return (
    <div>
        <div className='flex justify-center items-center bg-gray-900 p-6'>
            <div className=''>
                <div className='mb-5'>
                    <h1 className='text-3xl font-bold text-white '>Find the perfect <i className='font-sans font-semibold'>freelance</i> services for your business</h1>
                </div>
                            <Search
                                prefix={<SearchOutlined className='p-2' />}
                                placeholder="Try 'building mobile app' "
                                allowClear
                                enterButton="Search"
                                onSearch={onSearch}
                                autoComplete='off'
                                className='bg-green-500 font-semibold rounded-md'
                            /> 
            </div>          
        </div>
    </div>
  )
}
