import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import CardWork from '../../Component/CardWork/CardWork'
import CardWorkResponsive from '../../Component/CardWork/CardWorkResponsive'
import FilterResponsive from '../../Component/Fillter/FilterResponsive'
import { https } from '../../Services/configURL'


export default function DetailCategories() {
    let [detailCategories, setDetailCategories] = useState([])
    let params = useParams()

    useEffect(()=>{
        https.get(`/api/cong-viec/lay-cong-viec-theo-chi-tiet-loai/${params.id}`)
        .then((res) => {
            setDetailCategories(res.data.content)
        }).catch((err) => {
            console.log(err);
        });
    },[params.id])
    console.log(detailCategories)

    

  return (
    <div className='mt-24 flex justify-center'>
        <div className='w-11/12'>
            <div className='mt-10'>
                <FilterResponsive />
                <CardWorkResponsive workArr={detailCategories} />
            </div>
        </div>
    </div>
  )
}
