import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import { adminService } from "../../../../Services/adminService";
import UpdateUserInforForm from "./UpdateUserInforForm";
export default function UpdateUserInforComponent({ data }) {
  let { id } = data;
  const [open, setOpen] = useState(false);
  const [detailedData, setDetailedData] = useState("");
  const showModal = () => {
    setOpen(true);
    adminService
      .getThongTinNguoiDungChiTiet(id)
      .then((res) => {
        console.log(res.data.content);
        setDetailedData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const renderUpdatingForm = () => {
    if (detailedData) {
      return <UpdateUserInforForm dataUpdate={detailedData} />;
    }
  };
  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <div>
      <Button className="bg-orange-600 text-white" onClick={showModal}>
        Edit
      </Button>
      <Modal
        open={open}
        title="Cập nhật thông tin"
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Đóng
          </Button>,
        ]}
      >
        {" "}
        {renderUpdatingForm()}
      </Modal>
    </div>
  );
}
