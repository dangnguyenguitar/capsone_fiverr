import React from "react";
import { Button, Form, Input, Select, notification } from "antd";
import { adminService } from "../../../../Services/adminService";

export default function UpdateUserInforForm({ dataUpdate }) {
  let { id, name, email, phone, role } = dataUpdate;
  const onUpdateUser = (values) => {
    let updatingUser = { ...values, id };
    adminService
      .putCapNhatThongTinNguoiDung(updatingUser)
      .then((res) => {
        console.log(res);
        notification.success({
          placement: "topLeft",
          bottom: 50,
          duration: 3,
          rtl: true,
          description: "Cập nhật thông tin thành công! Reload trang sau 2s",
          message: "Thông báo!",
        });

        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const { Option } = Select;
  return (
    <div>
      <div className="pt-5">
        {" "}
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          onFinish={onUpdateUser}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            id: id,
            name: name,
            email: email,
            phone: phone,
          }}
        >
          <Form.Item label="id" name="id">
            <Input disabled />
          </Form.Item>
          <Form.Item
            label="Tên đăng nhập"
            name="name"
            rules={[
              {
                message: "Hãy nhập tên đăng nhập",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                message: "Please input your email!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Số điện thoại"
            name="phone"
            rules={[
              {
                message: "Hãy nhập số điện thoại",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Role"
            name="role"
            rules={[
              {
                message: "Hãy chọn chức vụ cho người dùng",
              },
            ]}
          >
            <Select defaultValue={role} onChange={(value) => {}}>
              <Option value="ADMIN">Admin</Option>
              <Option value="USER">User</Option>
            </Select>
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button className="bg-blue-600 text-white" htmlType="submit">
              Xác nhận
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
