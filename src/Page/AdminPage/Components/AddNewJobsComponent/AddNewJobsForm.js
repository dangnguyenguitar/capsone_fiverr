import React from "react";
import { Button, Form, Input, notification, InputNumber } from "antd";
import { adminService } from "../../../../Services/adminService";

const onFinish = (jobData) => {
  console.log("jobData: ", jobData);
  adminService
    .postThemCongViec(jobData)
    .then((res) => {
      console.log(res.data.content);
      notification.success({
        placement: "topLeft",
        bottom: 50,
        duration: 3,
        rtl: true,
        description: "Đã thêm 1 công việc! Tự động load trang sau 2s",
        message: "Thông báo!",
      });

      setTimeout(() => {
        window.location.reload();
      }, 1500);
    })
    .catch((err) => {
      console.log(err);
      notification.error({ description: `${err.response.data.content}` });
    });
};
const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

export default function AddNewJobsForm() {
  return (
    <div className="pt-5">
      {" "}
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Tên công việc"
          name="tenCongViec"
          rules={[
            {
              required: true,
              message: "Hãy nhập tên công việc",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Giá tiền"
          name="giaTien"
          rules={[{ required: true, message: "Hãy nhập giá tiền" }]}
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          label="Mã người tạo"
          name="nguoiTao"
          rules={[
            {
              required: true,
              message: "Hãy nhập mã người tạo",
            },
          ]}
        >
          <InputNumber />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-blue-600 text-white" htmlType="submit">
            Thêm
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
