import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import AddNewJobsForm from "./AddNewJobsForm";

export default function AddNewJobsComponent() {
  const [open, setOpen] = useState(false);
  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button className="bg-blue-600 text-white" onClick={showModal}>
        Thêm công việc mới{" "}
      </Button>
      <Modal
        open={open}
        title="Thêm công việc mới"
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        {" "}
        <AddNewJobsForm />
      </Modal>
    </div>
  );
}
