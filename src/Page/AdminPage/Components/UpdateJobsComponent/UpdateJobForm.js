import React from "react";
import { Button, Form, Input, InputNumber, notification } from "antd";
import { adminService } from "../../../../Services/adminService";

export default function UpdateJobForm({ dataUpdate }) {
  let { id, tenCongViec, giaTien } = dataUpdate;
  const onUpdateJob = (values) => {
    console.log("values: ", values);
    let updatingJob = { ...values, id };
    adminService
      .putCapNhatCongViec(updatingJob)
      .then((res) => {
        console.log(res);
        notification.success({
          placement: "topLeft",
          bottom: 50,
          duration: 3,
          rtl: true,
          description: "Cập nhật thông tin thành công! Reload trang sau 2s",
          message: "Thông báo!",
        });

        setTimeout(() => {
          window.location.reload();
        }, 1500);
      })
      .catch((err) => {
        console.log(err);
        notification.error({
          description: `${err.response.data.content}`,
        });
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <div className="pt-5">
        {" "}
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          onFinish={onUpdateJob}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          initialValues={{
            id: id,
            tenCongViec: tenCongViec,
            giaTien: giaTien,
          }}
        >
          <Form.Item label="id" name="id">
            <Input disabled />
          </Form.Item>
          <Form.Item
            label="Tên công viêc"
            name="tenCongViec"
            rules={[
              {
                message: "Hãy nhập tên công viêc",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="Giá tiền" name="giaTien">
            <InputNumber />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button className="bg-blue-600 text-white" htmlType="submit">
              Xác nhận
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
