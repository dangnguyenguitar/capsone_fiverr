import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import { adminService } from "../../../../Services/adminService";
import UpdateJobForm from "./UpdateJobForm";
export default function UpdateJobsComponent({ data }) {
  let { id } = data;
  const [open, setOpen] = useState(false);
  const [detailedData, setDetailedData] = useState("");
  const showModal = () => {
    setOpen(true);
    adminService
      .getCongViecChiTiet(id)
      .then((res) => {
        console.log("getcongviechitiet: ", res.data.content);
        setDetailedData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const renderUpdatingForm = () => {
    if (detailedData) {
      return <UpdateJobForm dataUpdate={detailedData} />;
    }
  };
  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <div>
      <Button className="bg-orange-600 text-white" onClick={showModal}>
        Edit
      </Button>
      <Modal
        open={open}
        title="Cập nhật thông tin"
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Đóng
          </Button>,
        ]}
      >
        {" "}
        {renderUpdatingForm()}
      </Modal>
    </div>
  );
}
