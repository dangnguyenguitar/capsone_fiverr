import React from "react";
import { Button, Form, Input, Select, notification } from "antd";
import { adminService } from "../../../../Services/adminService";
const onFinish = (values) => {
  adminService
    .postThemNguoiDung(values)
    .then((res) => {
      notification.success({
        placement: "topLeft",
        bottom: 50,
        duration: 3,
        rtl: true,
        description:
          "Đã thêm 1 người dùng mới thành công! Tự động load trang sau 3s",
        message: "Thông báo!",
      });

      setTimeout(() => {
        window.location.reload();
      }, 3000);
    })
    .catch((err) => {
      console.log(err);
    });
};
const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};
const { Option } = Select;

export default function AddNewUserForm() {
  return (
    <div className="pt-5">
      {" "}
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Tên đăng nhập"
          name="name"
          rules={[
            {
              required: true,
              message: "Hãy nhập tên đăng nhập",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[
            {
              required: true,
              message: "Hãy nhập mật khẩu",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Số điện thoại"
          name="phone"
          rules={[
            {
              required: true,
              message: "Hãy nhập số điện thoại",
            },
          ]}
        >
          <Input type="text" />
        </Form.Item>

        <Form.Item
          label="Role"
          name="role"
          rules={[
            {
              required: true,
              message: "Hãy chọn chức vụ cho người dùng",
            },
          ]}
        >
          <Select onChange={(value) => {}}>
            <Option value="ADMIN">Admin</Option>
            <Option value="USER">User</Option>
          </Select>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button className="bg-blue-600 text-white" htmlType="submit">
            Thêm
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
