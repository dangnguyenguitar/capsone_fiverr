import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import AddNewUserForm from "./AddNewUserForm";

export default function AddNewUserComponent() {
  const [open, setOpen] = useState(false);
  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  return (
    <div>
      <Button className="bg-blue-600 text-white" onClick={showModal}>
        Thêm quản trị viên{" "}
      </Button>
      <Modal
        open={open}
        title="Thêm quản trị viên"
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
        ]}
      >
        {" "}
        <AddNewUserForm />
      </Modal>
    </div>
  );
}
