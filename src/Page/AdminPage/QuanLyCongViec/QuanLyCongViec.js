import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { adminService } from "../../../Services/adminService";
import { Button, Space, Table, Popconfirm, Input, notification } from "antd";
import AddNewJobsComponent from "../Components/AddNewJobsComponent/AddNewJobsComponent";
import UpdateJobsComponent from "../Components/UpdateJobsComponent/UpdateJobsComponent";

const { Column } = Table;

export default function QuanLyCongViec() {
  const [displayData, setDisplayData] = useState([]);
  const [userList, setUserList] = useState([]);
  useEffect(() => {
    adminService
      .getDanhSachNguoiDung()
      .then((res) => {
        setUserList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  useEffect(() => {
    adminService
      .getDanhSachCongViec()
      .then((res) => {
        setDisplayData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  const handleDeleteJob = (record) => {
    adminService
      .deleteXoaCongViec(record.id)
      .then((res) => {
        console.log(res);
        notification.success({
          placement: "topLeft",
          bottom: 50,
          duration: 3,
          rtl: true,
          description: "Xoá người dùng thành công!",
          message: "Thông báo!",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
        notification.error({ description: `${err.response.data.content}` });
      });
  };

  const [query, setQuery] = useState("");

  return (
    <div>
      <div className="flex gap-5">
        <AddNewJobsComponent />
        <Input
          type="text"
          placeholder="Tìm kiếm tên công việc..."
          className=""
          onChange={(e) => {
            setQuery(e.target.value);
          }}
        />
      </div>
      <Table
        dataSource={displayData.filter((record) => {
          return record.tenCongViec.toLowerCase().includes(query);
        })}
        size="small"
        className="mt-5"
        rowKey={(record) => {
          return record.id;
        }}
      >
        <Column title="Id" dataIndex="id" key="id" />
        <Column
          title="Tên công việc"
          dataIndex="tenCongViec"
          key="tenCongViec"
        />

        <Column
          title="Người tạo"
          dataIndex="nguoiTao"
          key="nguoiTao"
          render={(record) => {
            //Hiển thị email của người tạo dựa theo api/users
            let x = userList.map((user) => {
              if (user.id === record) {
                return user.email;
              } else {
                return null;
              }
            });
            return x;
          }}
        />
        <Column title="Giá tiền" dataIndex="giaTien" key="giaTien" />
        <Column
          title="Action"
          key="action"
          render={(_, record) => (
            <Space size="middle">
              <UpdateJobsComponent data={record} />
              <Popconfirm
                placement="topLeft"
                title={"Thông báo!"}
                description={"Bạn có chắc chắn muốn xoá công việc này?"}
                onConfirm={() => {
                  handleDeleteJob(record);
                }}
                okText="Yes"
                okType="default bg-red-500 text-white"
                cancelText="No"
              >
                <Button danger>Delete</Button>
              </Popconfirm>
            </Space>
          )}
        />
      </Table>
    </div>
  );
}

// {
//     "id": 1,
//     "tenCongViec": "I will design an outstanding logo",
//     "danhGia": 100,
//     "giaTien": 15,
//     "nguoiTao": 1,
//     "hinhAnh": "https://fiverrnew.cybersoft.edu.vn/images/cv1.jpg",
//     "moTa": "\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.",
//     "maChiTietLoaiCongViec": 2,
//     "moTaNgan": "Plus - MOST SELLING!\r\nUS$65\r\n3 logo options + source file in Ai, EPS, SVG, PDF, and PSD\r\n\r\n2 Days Delivery\r\n5 Revisions\r\n3 concepts included\r\nLogo transparency\r\nVector file\r\nPrintable file\r\nSource file",
//     "saoCongViec": 1
// }
