import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { adminService } from "../../../Services/adminService";
import { Button, Space, Table, Popconfirm } from "antd";

const { Column } = Table;

export default function QuanLyLoaiCongViec() {
  const [displayData, setDisplayData] = useState([]);
  useEffect(() => {
    adminService
      .getDanhSachLoaiCongViec()
      .then((res) => {
        setDisplayData(res.data.content);
        console.log("res.data.content: ", res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  return (
    <div>
      <div className="flex gap-5"></div>
      <Table
        dataSource={displayData}
        size="small"
        className="mt-5"
        rowKey={(record) => {
          return record.id;
        }}
      >
        <Column title="Id" dataIndex="id" key="id" />
        <Column
          title="Tên loại công việc"
          dataIndex="tenLoaiCongViec"
          key="tenLoaiCongViec"
        />
        <Column
          title="Action"
          key="action"
          render={(_, record) => (
            <Space size="middle">
              <Popconfirm
                placement="topLeft"
                title={"Thông báo!"}
                description={"Bạn có chắc chắn muốn xoá công việc này?"}
                onConfirm={() => {}}
                okText="Yes"
                okType="default bg-red-500 text-white"
                cancelText="No"
              >
                <Button danger>Delete</Button>
              </Popconfirm>
            </Space>
          )}
        />
      </Table>
    </div>
  );
}
