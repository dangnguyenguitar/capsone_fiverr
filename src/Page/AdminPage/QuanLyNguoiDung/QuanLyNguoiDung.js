import React, { useEffect, useState } from "react";
import {
  Button,
  notification,
  Space,
  Table,
  Tag,
  Popconfirm,
  Input,
} from "antd";
import { adminService } from "../../../Services/adminService";
import AddNewUserComponent from "../Components/AddNewUserComponent/AddNewUserComponent";
import UpdateUserInforComponent from "../Components/UpdateUserInforComponent/UpdateUserInforComponent";
const { Column } = Table;

export default function QuanLyNguoiDung() {
  const [displayData, setDisplayData] = useState([]);

  useEffect(() => {
    adminService
      .getDanhSachNguoiDung()
      .then((res) => {
        setDisplayData(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });

    return () => {};
  }, []);

  const handleDeleteUser = (record) => {
    adminService
      .deleteXoaNguoiDung(record.id)
      .then((res) => {
        console.log(res);
        notification.success({
          placement: "topLeft",
          bottom: 50,
          duration: 3,
          rtl: true,
          description: "Xoá người dùng thành công!",
          message: "Thông báo!",
        });
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [query, setQuery] = useState("");

  return (
    <div>
      <div className="flex space-x-5">
        <AddNewUserComponent />
        <Input
          type="text"
          placeholder="Tìm kiếm tên hoặc email..."
          className=""
          onChange={(e) => {
            setQuery(e.target.value);
          }}
        />
      </div>
      <Table
        dataSource={displayData.filter((record) => {
          return (
            record.name.toLowerCase().includes(query) ||
            record.email.toLowerCase().includes(query)
          );
        })}
        size="small"
        className="mt-5"
        rowKey={(record) => {
          return record.id;
        }}
      >
        <Column title="Id" dataIndex="id" key="id" />
        <Column title="Tên đăng nhập" dataIndex="name" key="name" />
        <Column title="Email" dataIndex="email" key="email" />
        <Column
          title="Role"
          dataIndex="role"
          key="role"
          render={(role) => {
            if (role.toLowerCase() === "admin") {
              return (
                <Tag color="blue" key={role}>
                  {role}
                </Tag>
              );
            }
            if (role.toLowerCase() === "user") {
              return (
                <Tag color="green" key={role}>
                  {role}
                </Tag>
              );
            } else {
              return (
                <Tag color="red" key={role}>
                  {role}
                </Tag>
              );
            }
          }}
        />
        <Column
          title="Action"
          key="action"
          render={(_, record) => (
            <Space size="middle">
              <UpdateUserInforComponent data={record} />
              <Popconfirm
                placement="topLeft"
                title={"Thông báo!"}
                description={"Bạn có chắc chắn muốn xoá người dùng này?"}
                onConfirm={() => {
                  handleDeleteUser(record);
                }}
                okText="Yes"
                okType="default bg-red-500 text-white"
                cancelText="No"
              >
                <Button danger>Delete</Button>
              </Popconfirm>
            </Space>
          )}
        />
      </Table>
    </div>
  );
}
