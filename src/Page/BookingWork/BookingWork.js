import { message } from 'antd'
import moment from 'moment/moment'
import React from 'react'
import { useParams } from 'react-router-dom'
import swal from 'sweetalert'
import { userService } from '../../Services/userService'

export default function BookingWork({ detailWork }) {
  let params = useParams()
  console.log(params.id)

  let employWork = () => {
    if(!localStorage.getItem("USER_LOGIN")) {
      swal({
        title: "You are not logged in",
        text: "Please login to comment!",
        icon: "warning",
        buttons: "OK"
      })
    } else {
        let inforEmploy = {
          id: 0,
          maCongViec: params.id,
          maNguoiThue: JSON.parse(localStorage.getItem("USER_LOGIN")).user.id,
          ngayThue: moment().format("DD/MM/YYYY"),
          hoanThanh: true,
        }
        console.log(inforEmploy);
        userService.employ(inforEmploy)
        .then((res) => {
          swal({
            title:'Success',
            icon: 'success',
            buttons: 'OK'
          })
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        }).catch((err) => {
          message.error("Error")
          console.log(err);
        });
    }
  }

  let renderFormBooking = () => {
    return detailWork.map((work)=>{
      return (
        <div className='border-2 bg-white'>
          <div className='flex'>
            <p className='text-green-600 text-2xl font-bold w-full text-center p-3 border-b-2 border-green-600'>Standard</p>
          </div>
          <div className='p-6'>
            <div className='flex justify-between'>
              <p className='font-bold text-lg'>Standard</p>
              <p className='text-xl'>${work.congViec.giaTien}</p>
            </div>
            <p className='my-5 text-base'>{work.congViec.moTaNgan}</p>
            <div className='flex justify-center'>
              <button onClick={()=>{employWork()}} className='bg-green-600 text-white font-bold text-xl w-full rounded-md p-2'>Continue (${work.congViec.giaTien})</button>
            </div>
          </div>
        </div>
      )
    })
  }
    
  return (
    <div className=''>
        {renderFormBooking()}
    </div>
  )
}
