import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import Login from './Login'
import LoginMobile from './LoginMobile'
import LoginTablet from './LoginTablet'

export default function LoginResponsive() {
  return (
    <div>
        <Desktop><Login/></Desktop>
        <Tablet><LoginTablet/></Tablet>
        <Mobile><LoginMobile/></Mobile>
    </div>
  )
}


