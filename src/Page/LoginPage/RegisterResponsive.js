import React from 'react'
import { Desktop, Mobile } from '../../HOC/Responsive'
import Register from './Register'
import RegisterMobile from './RegisterMobile'

export default function RegisterResponsive() {
  return (
    <div>
        <Desktop><Register/></Desktop>
        <Mobile><RegisterMobile/></Mobile>
    </div>
  )
}
