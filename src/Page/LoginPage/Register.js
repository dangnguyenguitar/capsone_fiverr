import { Form, Input, Radio, Button, message } from 'antd'
import React from 'react'
import { NavLink, useNavigate } from 'react-router-dom';
import { userService } from '../../Services/userService';

export default function Register() {
  let navigate = useNavigate()
  const onFinish = (values) => {
    console.log(values);
    userService.dangKi(values)
    .then((res) => {
      message.success('Successful registration')
      setTimeout(() => {
        navigate('/login')
      }, 700);
    }).catch((err) => {
      message.error('Registration failed')
    });
  }

  const formItemLayout = {
    labelCol: {
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
  return (
    <div className='translate-x-1/2 translate-y-11 w-1/2 flex justify-center p-5 bg-blue-100 rounded-md shadow-xl '>
      <div className='w-1/3'>
        <span className='text-3xl font-semibold'>Sign Up</span>
        <img className='mt-20 shadow-lg' src="https://colorlib.com/etc/regform/colorlib-regform-7/images/signin-image.jpg" alt="" />
      </div>
      <div className='w-2/3 p-2'>
        <Form
        {...formItemLayout}
        name="register"
        onFinish={onFinish}
        initialValues={{
          id: 0,
          gender: true,
          role: 'USER',
          skill: [],
          certification: [],
        }}
        >
          <Form.Item name='id'></Form.Item>
          <Form.Item name='skill'></Form.Item>
          <Form.Item
          name='name'
          label='Name'
          rules={[
            {
              required: true,
              message: 'Please input your name!',
            },
          ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item
          name='email'
          label='E-mail'
          rules={[
            {
              required: true,
              message: 'Please input your E-mail !',
            },
            {
              type: 'email',
              message: 'Please input a valid email',
            },
          ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item
          name='password'
          label='Password'
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
          >
            <Input.Password/>
          </Form.Item>

          <Form.Item
          name='phone'
          label='Phone'
          rules={[
            {
              required: true,
              message: 'Please input your phone!',
            },
          ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item name="birthday" label="Birthday" rules={[
            {
              required: true,
              message: 'Please input your birthday!',
            },
          ]}>
            <Input placeholder='DD/MM/YYYY'/>
          </Form.Item>

          <Form.Item name='gender' label="Gender">
            <Radio.Group>
              <Radio value = {true}> Male </Radio>
              <Radio value={false}> Female </Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item name='certification'></Form.Item>
          <Form.Item name='role'></Form.Item>
          
          <Form.Item className='-mt-28' {...tailFormItemLayout}>
            <Button className="bg-blue-500 text-white hover:bg-white mr-14" htmlType="submit">
              Register
            </Button>
            <NavLink className={'border-b-2 border-blue-600'} to={'/login'}>Are you have account?</NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

