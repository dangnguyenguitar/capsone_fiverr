import React, { useState } from "react";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input, message, Modal } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userService } from "../../Services/userService";
import { setUserInfor } from "../../redux-toolkit/slice/userSlice";
import { userLocalService } from "../../Services/localStorageService";
import animationLogin from "../../assets/72874-user-profile-v2.json";
import Lottie from "lottie-react";

export default function Login() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        console.log(res);
        dispatch(setUserInfor(res.data.content));
        message.success("Logged in successfully");
        userLocalService.set(res.data.content);
        setTimeout(() => {
          navigate('/');
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Login failed");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onFinishResetPassword = (values) => {
    console.log(values);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className="w-screen h-screen flex justify-center items-center bg-slate-100">
      <div className="w-2/3 h-2/3 flex justify-center items-center rounded-md shadow-lg bg-white">
        <div className="container flex justify-center items-center">
          <div className="h-full w-1/2 bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500">
            <Lottie animationData={animationLogin} loop={true} />
          </div>
          <div className="h-full w-1/2">
            <Form
              name="login"
              className="ml-14"
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 20,
              }}
              layout="vertical"
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Please input your E-mail !",
                  },
                  {
                    type: "email",
                    message: "Nhập đúng định dạng E-mail!",
                  },
                ]}
              >
                <Input prefix={<MailOutlined />} placeholder="E-mail" />
              </Form.Item>

              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Please input your Password!",
                  },
                ]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Checkbox className="mr-20">Remember me</Checkbox>
                <NavLink
                  onClick={showModal}
                  className="text-blue-500 border-b"
                  to={"#"}
                >
                  Forgot password?
                </NavLink>
                <Modal
                  className="text-center"
                  title="Reset Password"
                  open={isModalOpen}
                  onCancel={handleCancel}
                  footer={""}
                >
                  <p>
                    Please enter your email address and we'll send you a link to
                    reset your password.
                  </p>
                  <Form onFinish={onFinishResetPassword}>
                    <Form.Item
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: "Please input your E-mail !",
                        },
                        {
                          type: "email",
                          message: "Please input a valid email",
                        },
                      ]}
                    >
                      <Input prefix={<MailOutlined />} placeholder="E-mail" />
                    </Form.Item>
                    <Form.Item>
                      <Button
                        htmlType="submit"
                        className="bg-blue-500 text-white hover:bg-white"
                      >
                        Submit
                      </Button>
                    </Form.Item>
                  </Form>
                </Modal>
              </Form.Item>

              <Form.Item className="flex">
                <Button
                  htmlType="submit"
                  className="bg-blue-500 text-white hover:bg-white w-full mb-2"
                >
                  Log in
                </Button>
                <NavLink className="text-blue-500 border-b" to={"/register"}>
                  Register now!
                </NavLink>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
