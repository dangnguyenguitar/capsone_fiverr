import { Form, Input, Radio, Button, message } from 'antd'
import React from 'react'
import { NavLink, useNavigate } from 'react-router-dom';
import { userService } from '../../Services/userService';

export default function RegisterMobile() {

    let navigate = useNavigate()
  const onFinish = (values) => {
    console.log(values);
    userService.dangKi(values)
    .then((res) => {
      message.success('Successful registration')
      setTimeout(() => {
        navigate('/login')
      }, 700);
    }).catch((err) => {
      message.error('Registration failed')
    });
  }

  const formItemLayout = {
    labelCol: {
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
  return (
    <div className='bg-[#f1fdf7] min-h-screen p-5'>
        <div className='p-3'>
        <NavLink to={'/'}><p className="mb-3 font-extrabold text-transparent text-2xl bg-clip-text bg-gradient-to-r from-purple-400 to-pink-600">Home</p></NavLink>
            <Form
            {...formItemLayout}
            name="register"
            onFinish={onFinish}
            initialValues={{
            id: 0,
            gender: true,
            role: 'USER',
            skill: [],
            certification: [],
            }}
            className='-mt-20'
            >
            <Form.Item name='id'></Form.Item>
            <Form.Item name='skill'></Form.Item>
            <Form.Item
            name='name'
            
            rules={[
                {
                required: true,
                message: 'Please input your name!',
                },
            ]}
            >
                <Input placeholder='Name'/>
            </Form.Item>

            <Form.Item
            name='email'
            rules={[
                {
                required: true,
                message: 'Please input your E-mail !',
                },
                {
                type: 'email',
                message: 'Please input a valid email',
                },
            ]}
            >
                <Input placeholder='E-mail'/>
            </Form.Item>

            <Form.Item
            name='password'
            rules={[
                {
                required: true,
                message: 'Please input your password!',
                },
            ]}
            >
                <Input.Password placeholder='Password'/>
            </Form.Item>

            <Form.Item
            name='phone'
            rules={[
                {
                required: true,
                message: 'Please input your phone!',
                },
            ]}
            >
                <Input placeholder='Phone' />
            </Form.Item>

            <Form.Item name="birthday" rules={[
                {
                required: true,
                message: 'Please input your birthday!',
                },
            ]}>
                <Input placeholder='Birthday: DD/MM/YYYY'/>
            </Form.Item>

            <Form.Item name='gender'>
                <Radio.Group>
                <Radio value = {true}> Male </Radio>
                <Radio value={false}> Female </Radio>
                </Radio.Group>
            </Form.Item>

            <Form.Item name='certification'></Form.Item>
            <Form.Item name='role'></Form.Item>
            
            <Form.Item className='-mt-28' {...tailFormItemLayout}>
                <Button className="bg-blue-500 text-white hover:bg-white mr-14" htmlType="submit">
                Register
                </Button>
                <NavLink className={'border-b-2 border-blue-600'} to={'/login'}>Are you have account?</NavLink>
            </Form.Item>
            </Form>
        </div>
    </div>
  )
}
