import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { https } from '../../Services/configURL';
import EditInforUser from './EditInforUser/EditInforUser';
import EditUserSkill from './EditUserSkill/EditUserSkill';
import EditCertification from './EditCertification/EditCertification';
import ListHiredJobs from './ListHiredJobs/ListHiredJobs';

export default function UserInfo() {
    let params = useParams()
    let [userInfor, setUserInfor] = useState([])
    useEffect(()=>{
        https.get(`/api/users/${params.id}`)
        .then((res) => {
            setUserInfor(res.data.content)
        }).catch((err) => {
            console.log(err)
        });
    },[params.id])

    console.log(userInfor);


    let renderAvatar = () => {
        if(userInfor.avatar === ''){
            return <></>
        }
        else {
            return <img style={{
                borderRadius: '50%',                                    
            }} className='w-full h-full object-cover' src={userInfor.avatar} alt="" />
        }
    }
  return (
    <div className='mt-28 flex justify-center bg-gray-100'>
        <div className='w-9/12 mt-10'>
            <div className='flex'>
                <div className='w-1/3 mr-28'>
                    <div className='bg-white p-8 border-2'>
                        <div className='border-b-2 border-gray-300 text-center'>
                            <div style={{
                                backgroundImage: 'url("https://media.istockphoto.com/id/1300845620/vector/user-icon-flat-isolated-on-white-background-user-symbol-vector-illustration.jpg?s=612x612&w=0&k=20&c=yBeyba0hUkh14_jgv1OKqIH0CCSWU_4ckRkAoy2p73o=")',
                                backgroundSize: 'cover',
                            }}
                                className='w-60 h-60 ml-5'
                            >
                                {renderAvatar()}
                            </div>
                            <p>{userInfor.name}</p>
                            <div className='mb-5'>
                                <button><EditInforUser userInfor={userInfor} /></button>
                            </div>
                        </div>
                        <div className='my-3'>
                            <div className='flex justify-between my-3'>
                                <span>From</span>
                                <span>Viet Nam</span>
                            </div>
                            <div className='flex justify-between'>
                                <span>Member since</span>
                                <span>{userInfor.birthday}</span>
                            </div>
                        </div>
                    </div>
                    <div className='mt-10 bg-white h-80 p-8 border-2'>
                        <div className='border-b-2 border-gray-300'>
                            <EditUserSkill userInfor = {userInfor} />
                        </div>
                        <div className='border-b-2 border-gray-300'>
                            <EditCertification userInfor = {userInfor} />
                        </div>
                    </div>
                </div>
                <div className='w-2/3 bg-white h-auto'>
                    <ListHiredJobs />
                </div>
            </div>
        </div>
    </div>
  )
}
