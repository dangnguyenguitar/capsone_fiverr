import React, { useState } from 'react'
import { Form, Input, Modal,Button, message } from 'antd';
import axios from 'axios';
import { BASE_URL, createConfig } from '../../../Services/configURL';
import { DeleteTwoTone } from '@ant-design/icons';

export default function EditUserSkill({ userInfor }) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

    const onFinish = (values) => {
      userInfor.skill?.push(values.skill)
      console.log(userInfor.skill)
      values.skill = userInfor.skill
      axios ({
        url: `${BASE_URL}/api/users/${userInfor.id}`,
        method: 'PUT',
        data: {
          id: 0,
                  name: userInfor.name,
                  email: userInfor.email,
                  phone: userInfor.phone,
                  birthday: userInfor.birthday,
                  gender: userInfor.gender,
                  role: userInfor.role,
                  certification: userInfor.certification,
                  skill: values.skill,
        },
        headers: createConfig(),
      })
      .then((res) => {
        message.success('Success')
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }).catch((err) => {
        message.error('Error')
        console.log(err);
      });
    }

    let deleteSkill = (skill) => {
        let index = userInfor.skill?.findIndex((element)=>{
          return skill === element;
        })
        
        userInfor.skill?.splice(index, 1)
        console.log(userInfor.skill)
        axios ({
          url: `${BASE_URL}/api/users/${userInfor.id}`,
          method: 'PUT',
          data: {
            id: 0,
                    name: userInfor.name,
                    email: userInfor.email,
                    phone: userInfor.phone,
                    birthday: userInfor.birthday,
                    gender: userInfor.gender,
                    role: userInfor.role,
                    certification: userInfor.certification,
                    skill: userInfor.skill,
          },
          headers: createConfig(),
        })
        .then((res) => {
          message.success('Success')
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        }).catch((err) => {
          message.error('Delete Fail')
          console.log(err);
        });
    }
  
      let renderSkill = () => {
          if (userInfor.skill?.length === 0) {
              return (
                  <p className='text-gray-400'>Add your Skills</p>
              )
          }else {
              return userInfor.skill?.map((skill)=>{
                return (
                  <div className='flex justify-between mx-2'>
                    <p className='text-gray-600' >+ {skill}</p>
                    <p className='text-lg mb-1 cursor-pointer'><DeleteTwoTone onClick={()=> {deleteSkill(skill)}} /></p>
                  </div>
                )
              })
          }
      }

  return (
    <div>
        <div className='flex justify-between'>
            <span>Skills</span>
            <span className='text-blue-500 cursor-pointer' onClick={showModal}>Add New</span>
            <Modal title="Add your Skill" open={isModalOpen} onCancel={handleCancel} footer=''>
              <Form
                onFinish={onFinish}
                >
                  <Form.Item
                    name='skill'
                    label='Skill'
                    rules={[
                      {
                        required: true,
                        message: 'Please input your Skill!',
                      },
                    ]}
                  >
                    <Input/>
                  </Form.Item>
                  <Form.Item 
                  >
                    <Button htmlType="submit" className="bg-blue-500 text-white hover:bg-white">
                      Add Skill
                    </Button>
                  </Form.Item>
                </Form>
            </Modal>
        </div>
        <div>
            {renderSkill()}
        </div>
    </div>
  )
}
