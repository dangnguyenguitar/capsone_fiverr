import React, { useEffect, useState } from 'react'
import { https } from '../../../Services/configURL'
import { StarFilled } from '@ant-design/icons';
import { Button, Empty } from 'antd';
import { NavLink } from 'react-router-dom';

export default function ListHiredJobs() {
    let [list, setList] = useState([])
    useEffect(()=>{
        https.get('/api/thue-cong-viec/lay-danh-sach-da-thue')
        .then((res) => {
            setList(res.data.content)
        }).catch((err) => {
            console.log(err);
        });
    },[])
    console.log(list)
    let renderList = () => {
        if (list.length === 0) {
            return (
                <Empty
                    image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    imageStyle={{
                    height: 60,
                    }}
                    description={
                    <span>
                        List is currently empty
                    </span>
                    }
                >
                    <NavLink to={'/'}><Button>Create Now</Button></NavLink>
                </Empty>
            )
        }else {
            return list.map((item) => {
                return (
                    <div className='p-3 border-y border-gray-300 flex'>
                        <img className='object-cover w-72 h-40' src={item.congViec.hinhAnh} alt="" />
                        <p className='mx-10 w-1/2'>{item.congViec.moTaNgan}</p>
                        <span className='font-semibold text-xl text-orange-500 flex justify-center'>
                            <StarFilled className='mt-1 mr-1' />
                            {item.congViec.saoCongViec}
                        </span>
                    </div>
                )
            })
        }
    }
  return (
    <div>
        {renderList()}
    </div>
  )
}
