import React, { useEffect, useState } from 'react'
import { https } from '../../../Services/configURL'
import { StarFilled } from '@ant-design/icons';
import { Button, Empty } from 'antd';
import { NavLink } from 'react-router-dom';

export default function ListHiredJobs() {
    let [list, setList] = useState([])
    useEffect(()=>{
        https.get('/api/thue-cong-viec/lay-danh-sach-da-thue')
        .then((res) => {
            setList(res.data.content)
        }).catch((err) => {
            console.log(err);
        });
    },[])
    console.log(list)
    let renderList = () => {
        if (list.length === 0) {
            return (
                <Empty
                    image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                    imageStyle={{
                    height: 60,
                    }}
                    description={
                    <span>
                        List is currently empty
                    </span>
                    }
                >
                    <NavLink to={'/'}><Button>Create Now</Button></NavLink>
                </Empty>
            )
        }else {
            return list.map((item) => {
                return (
                    <div key={item.id} className='p-2 border-y border-gray-300 flex'>
                        <NavLink to={`/detail-work/${item.id}`}><img className='object-cover w-28 h-20' src={item.congViec.hinhAnh} alt="" /></NavLink>
                        <p className='mx-2 w-1/2'>{item.congViec.moTaNgan.length<100?item.congViec.moTaNgan:item.congViec.moTaNgan.slice(0, 60)+"..."}</p>
                        <span className='font-semibold text-orange-500 flex justify-center'>
                            <StarFilled className='mt-1 mr-1 ml-2' />
                            {item.congViec.saoCongViec}
                        </span>
                    </div>
                )
            })
        }
    }
  return (
    <div>
        {renderList()}
    </div>
  )
}

// 