import React from 'react'
import { Desktop, Mobile, Tablet } from '../../../HOC/Responsive'
import ListHiredJobs from './ListHiredJobs'
import ListHiredJobsMobile from './ListHiredJobsMobile'
import ListHiredJobsTablet from './ListHiredJobsTablet'

export default function ListHiredJobsResponsive() {
  return (
    <div>
        <Desktop><ListHiredJobs/></Desktop>
        <Tablet><ListHiredJobsTablet/></Tablet>
        <Mobile><ListHiredJobsMobile/></Mobile>
    </div>
  )
}
