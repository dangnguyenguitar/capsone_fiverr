import React, { useState } from 'react'
import { Form, Input, Modal, Button, message } from 'antd';
import axios from 'axios';
import { BASE_URL, createConfig } from '../../../Services/configURL';
import { DeleteTwoTone } from '@ant-design/icons';

export default function EditCertification({ userInfor }) {
  console.log(userInfor)
    const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

    let renderCertification = () => {
        if (userInfor.certification?.length === 0) {
            return (
                <p className='text-gray-400'>Add your Certification</p>
            )
        }else {
          return userInfor.certification?.map((certification)=>{
            return (
              <div className='flex justify-between mx-2'>
                <p className='text-gray-600' >+ {certification}</p>
                <p className='text-lg mb-1 cursor-pointer'><DeleteTwoTone onClick={()=>{deleteCertification(certification)}} /></p>
              </div>
            )
          })
        }
    }

    const onFinish = (values) => {
      userInfor.certification?.push(values.certification)
      values.certification = userInfor.certification
      console.log(values)
      axios ({
        url: `${BASE_URL}/api/users/${userInfor.id}`,
        method: 'PUT',
        data: {
          id: 0,
                  name: userInfor.name,
                  email: userInfor.email,
                  phone: userInfor.phone,
                  birthday: userInfor.birthday,
                  gender: userInfor.gender,
                  role: userInfor.role,
                  skill: userInfor.skill,
                  certification: values.certification
        },
        headers: createConfig(),
      })
      .then((res) => {
        message.success('Success')
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }).catch((err) => {
        message.error('Error')
        console.log(err);
      });
    }

    let deleteCertification = (certification) => {
      let index = userInfor.certification?.findIndex((element)=>{
        return certification === element;
      })
      
      userInfor.certification?.splice(index, 1)
      axios ({
        url: `${BASE_URL}/api/users/${userInfor.id}`,
        method: 'PUT',
        data: {
          id: 0,
                  name: userInfor.name,
                  email: userInfor.email,
                  phone: userInfor.phone,
                  birthday: userInfor.birthday,
                  gender: userInfor.gender,
                  role: userInfor.role,
                  certification: userInfor.certification,
                  skill: userInfor.skill,
        },
        headers: createConfig(),
      })
      .then((res) => {
        message.success('Success')
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }).catch((err) => {
        message.error('Delete Fail')
        console.log(err);
      });
    }
    
  return (
    <div>
        <div className='flex justify-between'>
            <span>Certification</span>
            <span className='text-blue-500 cursor-pointer' onClick={showModal}>Add New</span>
            <Modal title="Add your Certification" open={isModalOpen} onCancel={handleCancel} footer=''>
                <Form
                onFinish={onFinish}
                >
                  <Form.Item
                    name='certification'
                    label='Certification'
                    rules={[
                      {
                        required: true,
                        message: 'Please input your Certification!',
                      },
                    ]}
                  >
                    <Input/>
                  </Form.Item>
                  <Form.Item 
                  >
                    <Button htmlType="submit" className="bg-blue-500 text-white hover:bg-white">
                      Add Certification
                    </Button>
                  </Form.Item>
                </Form>
            </Modal>
        </div>
        <div>
            {renderCertification()}
        </div>
    </div>
  )
}
