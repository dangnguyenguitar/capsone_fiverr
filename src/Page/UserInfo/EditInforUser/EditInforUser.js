import React, { useState } from 'react'
import { EditOutlined } from '@ant-design/icons';
import { Form, Input, Modal, Radio, Button, } from 'antd';
import axios from 'axios';
import { BASE_URL, createConfig } from '../../../Services/configURL';
import swal from 'sweetalert';


export default function EditInforUser({ userInfor }) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onFinish = (values) => {
    console.log(values);
    axios ({
      url: `${BASE_URL}/api/users/${userInfor.id}`,
      method: 'PUT',
      data: values,
      headers: createConfig(),
    })
    .then((res) => {
      swal({
        title: "Edit information successfully",
        icon: "success",
      })
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }).catch((err) => {
      swal({
        title: err.response.data,
        text: "Fill in the information again!",
        icon: "warning",
        button: "OK",
      });
      console.log(err);
    });
  }

  const formItemLayout = {
    labelCol: {
      sm: {
        span: 6,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 6,
      },
    },
  };

  return (
    <div>
        <p onClick={showModal}>
            <EditOutlined className='font-semibold text-xl' />
        </p>
        <Modal title="Information" open={isModalOpen} onCancel={handleCancel} footer=''>
            <Form
              {...formItemLayout}
              onFinish={onFinish}
              initialValues={{
                id: 0,
                avatar: '',
                name: userInfor.name,
                email: userInfor.email,
                phone: userInfor.phone,
                birthday: userInfor.birthday,
                gender: userInfor.gender,
                role: userInfor.role,
                certification: userInfor.certification,
                skill: userInfor.skill,
              }}
            >
              <div className='flex'>
                <Form.Item name='certification'></Form.Item>
                <Form.Item name='role'></Form.Item>
              </div>
              
              <Form.Item
                  label='Name'
                  name="name"
                >
                  <Input />
              </Form.Item>

              <Form.Item
                  label='Email'
                  name="email"
                >
                  <Input />
              </Form.Item>

              <Form.Item
                  label='Phone'
                  name="phone"
                >
                  <Input />
              </Form.Item>

              <Form.Item
                  label='Birthday'
                  name="birthday"
                >
                  <Input />
              </Form.Item>

              <Form.Item name='gender' label="Gender">
                <Radio.Group>
                  <Radio value = {true}> Male </Radio>
                  <Radio value={false}> Female </Radio>
                </Radio.Group>
              </Form.Item>

              <div className='flex'>
                <Form.Item name='skill'></Form.Item>
                <Form.Item name='id'></Form.Item>
              </div>

              <Form.Item className='-mt-10' {...tailFormItemLayout}>
                <Button className="bg-blue-500 text-white hover:bg-white w-full font-semibold text-base" htmlType="submit">
                  Update
                </Button>
              </Form.Item>
            </Form>
        </Modal>
    </div>
  )
}
