import React from 'react'
import { Desktop, Mobile, Tablet } from '../../HOC/Responsive'
import UserInfo from './UserInfo'
import UserInforMobile from './UserInforMobile'
import UserInfoTablet from './UserInfoTablet'

export default function UserInfoResponsive() {
  return (
    <div>
        <Desktop><UserInfo/></Desktop>
        <Tablet><UserInfoTablet/></Tablet>
        <Mobile><UserInforMobile/></Mobile>
    </div>
  )
}
