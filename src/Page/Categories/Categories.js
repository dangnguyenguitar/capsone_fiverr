import React, { useEffect, useRef, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { https } from '../../Services/configURL'
import { Empty } from 'antd';
import { BannerGraphicsDesign } from "../../constants/images";
import { Modal } from 'antd';
import { Player } from 'video-react';
import "video-react/dist/video-react.css";
import { PlayCircleFilled, ArrowRightOutlined  } from '@ant-design/icons';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import {
  Logo_design_2x,
  Architecture_Interior,
  WebDesign,
  Illustration_2x,
  PhotoshopEditing_2x,
  T_Shirts_Merchandise_2x,
  Product_industrial,
  SocialMediaDesign_2x,
  NftArt,
} from "../../constants/images";

import "./styles.css";

// import required modules
import { FreeMode, Navigation } from "swiper";

export default function Categories() {
    let [category, setCategory] = useState([])
    let params = useParams()
    let player = useRef();
    useEffect(()=>{
        https.get(`/api/cong-viec/lay-chi-tiet-loai-cong-viec/${params.id}`)
        .then((res) => {
            setCategory(res.data.content)
        }).catch((err) => {
            console.log(err);
        });
    },[params.id])

    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
      setIsModalOpen(true);
    };

    const handleCancel = () => {
      setIsModalOpen(false);
    };

    let stopVideo = () => {
      if (player.current !== undefined) {
        player.current.pause()
      }
    }

    console.log(category)
    let renderCategoryItem = () =>{
      return category.map((items)=>{
          if (items.dsNhomChiTietLoai.length === 0) {
            return (
                <div key={items.id}>
                  <h1 className='text-gray-600 text-xl font-bold'>Explore {items.tenLoaiCongViec}</h1>
                  <Empty />
                </div>
            )
          } else {
            return (
              <div key={items.id}>
                <div className='my-5'>
                  <h1 className='text-gray-600 text-2xl font-bold'>Explore {items.tenLoaiCongViec}</h1>
                </div>
                <div className='grid grid-cols-4 gap-10 my-5'>
                  {items.dsNhomChiTietLoai?.map((element)=>{
                    return (
                      <div key={element.id}>
                        <img className='w-80 h-52 object-cover rounded-xl' src={element.hinhAnh} alt="" />
                        <p className='text-gray-800 text-xl font-semibold my-4'>{element.tenNhom}</p>
                        {element.dsChiTietLoai?.map((item)=>{
                          return (
                            <NavLink key={item.id} to={`/detail-categories/${item.id}`}>
                              <p className='my-2 text-gray-600 text-lg hover:bg-slate-200 px-1 py-2'>{item.tenChiTiet}</p>
                            </NavLink>
                          )
                        })}
                      </div>
                    )
                  })}
                </div>
              </div>
            )
          }
      })
    }

    console.log(player)
  return (
    <div className='flex justify-center mt-32'>
      <div className='w-11/12'>
        <div className='relative my-8'>
          <img src={BannerGraphicsDesign} alt="" />
          <div className='absolute top-1/3 text-white left-1/2 -translate-x-1/2 -translate-y-1/2'>
            <h1 className='text-3xl font-bold'>Graphics & Design</h1>
            <p className='text-xl mt-2'>Designs to make you stand out.</p>
            <div className='ml-8 mt-6'>
              <button onClick={showModal}
                className='flex justify-center items-center border border-lime-50 px-5 py-2 text-lg font-normal rounded-md
                hover:bg-white hover:text-green-900'
              >
                <PlayCircleFilled className='mr-2 mt-1' /> How Fiverr Works
              </button>
              <Modal afterClose={stopVideo()} width={850} open={isModalOpen} onCancel={handleCancel} footer=''>
                <div style={{
                  width: '107%'
                }}>
                  <Player
                    className='absolute top-0 left-0 -my-5 -ml-6'
                    autoPlay
                    ref={player}
                  >
                    <source src='https://fiverr-res.cloudinary.com/video/upload/t_fiverr_hd_nl/v1/video-attachments/generic_asset/asset/ab0907217c9f9a2c1d2eee677beb7619-1626082923646/how_fiverr_works' type="video/mp4" />
                  </Player>
                </div>
              </Modal>
            </div>
          </div>
          <div className='mt-10'>
            <h3 className='font-bold text-2xl text-gray-700'>Most popular in Graphics & Design</h3>
            <div className=''>
              <Swiper
                slidesPerView={4.5}
                spaceBetween={10}
                freeMode={true}
                navigation={false}
                modules={[FreeMode, Navigation]}
                className="mySwiper"
              >
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={Logo_design_2x} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Minimalist Logo Design</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={Architecture_Interior} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Architecture & Interior Design</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={WebDesign} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Website Design</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1 w-full'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={Illustration_2x} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Illustration</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={PhotoshopEditing_2x} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Image Editing</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={T_Shirts_Merchandise_2x} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>T-Shirts & Merchandise</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={Product_industrial} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Industrial & Product Design</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={SocialMediaDesign_2x} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>Social Media Design</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
                <SwiperSlide 
                className='rounded-xl my-5 ml-1'
                style={{
                  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px'
                }}>
                  <a className='flex justify-around items-center w-auto p-2' href="/#">
                    <img className='w-12 h-12 object-cover mr-3' src={NftArt} alt="" />
                    <span className='font-semibold text-gray-800 mr-2'>NFT Art</span>
                    <span className='mb-1 text-gray-700'><ArrowRightOutlined /></span>
                  </a>
                </SwiperSlide>
            </Swiper>
            </div>
          </div>
        </div>
        {renderCategoryItem()}
      </div>
    </div>
  )
}
