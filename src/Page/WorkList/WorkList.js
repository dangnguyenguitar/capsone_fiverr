
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import CardWorkResponsive from '../../Component/CardWork/CardWorkResponsive'
import FilterResponsive from '../../Component/Fillter/FilterResponsive'
import { https } from '../../Services/configURL'

export default function WorkList() {
    let params = useParams()
    let [workByName, setWorkByName] = useState([])


    useEffect(()=>{
        https.get(`/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${params.name}`)
        .then((res) => {
            setWorkByName(res.data.content)            
        }).catch((err) => {
            console.log(err)
        });
    },[params.name])

    console.log(workByName);

  return (
    <div className='flex justify-center mt-32'>
        <div className='w-11/12'>
            <FilterResponsive />
            <CardWorkResponsive workArr={workByName} />
        </div>
    </div>
  )
}
