import logoDesign from "../assets/images/logo-design.jpeg"
import voiceOver from "../assets/images/voice-over.jpeg"
import socialMedia from "../assets/images/social-media.jpeg"
import videoExplaner from "../assets/images/video-exp.jpeg"
import wordPress from "../assets/images/word-press.jpeg"
import articalFiverr from "../assets/images/article-fiverr.webp"
import verifyIcon from "../assets/images/check.svg"
import articalFiverr2 from "../assets/images/image-2.jpg"
import graphicDesign from "../assets/images/graphic.svg"
import digitalMarketing from "../assets/images/digitalmkt.svg"
import writingTrans from "../assets/images/writingtrans.svg"
import videoAni from "../assets/images/videoani.svg"
import musicAudio from "../assets/images/musicaudio.svg"
import business from "../assets/images/business.svg"
import lifeStyle from "../assets/images/lifestyle.svg"
import data from "../assets/images/data.svg"
import programingTech from "../assets/images/program-tech.svg"
import bannerGraphicsDesign from '../assets/images/graphics-design-desktop.webp'
import logoDesign_2x from '../assets/images/Logo design_2x.webp'
import Architecture from '../assets/images/Architecture _ Interior Design_2x.webp'
import webDesign from '../assets/images/Web Design.webp'
import Illustration from '../assets/images/Illustration_2x.webp'
import imageEdit from '../assets/images/Photoshop Editing_2x.webp'
import TShirts from '../assets/images/T-Shirts _ Merchandise_2x.webp'
import industrial from '../assets/images/Product _ industrial design.webp'
import social from '../assets/images/Social Media Design_2x.webp'
import nft from '../assets/images/Nft Art (1).webp';


export const LOGO_DESIGN=logoDesign;
export const VOICE_OVER=voiceOver;
export const SOCIAL_MEDIA=socialMedia;
export const VIDEO_EXPLAINER=videoExplaner;
export const WORD_PRESS=wordPress;
export const ARTICLE_FIVERR=articalFiverr;
export const VERIFY_ICON=verifyIcon;
export const ARTICLE_FIVERR2=articalFiverr2;
export const GRAPHIC_DESIGN=graphicDesign;
export const DIGITAL_MARKETING=digitalMarketing;
export const WRITING_TRANSLATION=writingTrans;
export const VIDEO_ANIMATION=videoAni;
export const MUSIC_AUDIO=musicAudio;
export const PROGRAMING_TECH=programingTech;
export const BUSINESS=business;
export const LIFE_STYLE=lifeStyle;
export const DATA=data;
export const BannerGraphicsDesign = bannerGraphicsDesign
export const Logo_design_2x = logoDesign_2x
export const Architecture_Interior = Architecture
export const WebDesign = webDesign
export const Illustration_2x = Illustration
export const PhotoshopEditing_2x = imageEdit
export const T_Shirts_Merchandise_2x = TShirts
export const Product_industrial = industrial
export const SocialMediaDesign_2x = social
export const NftArt = nft

