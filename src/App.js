import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./HOC/Layout";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import WorkList from "./Page/WorkList/WorkList";
import Categories from "./Page/Categories/Categories";
import DetailCategories from "./Page/DetailCategories/DetailCategories";
import LayoutAdmin from "./HOC/LayoutAdmin/LayoutAdmin";
import QuanLyNguoiDung from "./Page/AdminPage/QuanLyNguoiDung/QuanLyNguoiDung";
import QuanLyCongViec from "./Page/AdminPage/QuanLyCongViec/QuanLyCongViec";
import QuanLyLoaiCongViec from "./Page/AdminPage/QuanLyLoaiCongViec/QuanLyLoaiCongViec";
import QuanLyDichVu from "./Page/AdminPage/QuanLyDichVu/QuanLyDichVu";
import Spinner from "./Component/Spinner/Spinner";
import HomePageResponsive from "./Page/HomePage/HomePageResponsive";
import DetailWorkResponsive from "./Page/DetailWork/DetailWorkResponsive";
import UserInfoResponsive from "./Page/UserInfo/UserInfoResponsive";
import LoginResponsive from "./Page/LoginPage/LoginResponsive";
import RegisterResponsive from "./Page/LoginPage/RegisterResponsive";

function App() {
  return (
    <div className="App">
      <Spinner />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePageResponsive />
              </Layout>
            }
          />
          <Route
            path="/categories/:id"
            element={
              <Layout>
                <Categories />
              </Layout>
            }
          />
          <Route
            path="/detail-categories/:id"
            element={
              <Layout>
                <DetailCategories />
              </Layout>
            }
          />
          <Route
            path="/work-list/:name"
            element={
              <Layout>
                <WorkList />
              </Layout>
            }
          />
          <Route
            path="/detail-work/:id"
            element={
              <Layout>
                <DetailWorkResponsive />
              </Layout>
            }
          />
          <Route path="/login" element={<LoginResponsive />} />
          <Route path="/register" element={<RegisterResponsive />} />
          <Route
            path="/user/:id"
            element={
              <Layout>
                <UserInfoResponsive />
              </Layout>
            }
          />
          <Route path="*" element={<NotFoundPage />} />
          <Route
            path="/admin/quanlynguoidung"
            element={
              <LayoutAdmin>
                <QuanLyNguoiDung />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/quanlycongviec"
            element={
              <LayoutAdmin>
                <QuanLyCongViec />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/quanlyloaicongviec"
            element={
              <LayoutAdmin>
                <QuanLyLoaiCongViec />
              </LayoutAdmin>
            }
          />
          <Route
            path="/admin/quanlydichvu"
            element={
              <LayoutAdmin>
                <QuanLyDichVu />
              </LayoutAdmin>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
